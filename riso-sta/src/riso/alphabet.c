#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>
#include"alphabet.h"
#include"global_constants.h"


/*====================================*/
void count_alpha_size(char *path)
/*====================================*/
{
	FILE *fp;
	char c;

	if ((fp = fopen(path, "r"))==NULL) {
		printf("Error: cannot open alphabet file %s\n",path);
		exit(1);
	}

	c = getc(fp);
        while (c != EOF && c!='\n' && c!='\r') c = getc(fp); 

	alpha_size=0;
	
	c = getc(fp);
	while (c != EOF) {
		while (c == '\n' || c=='\r') c = getc(fp);
		if (c == EOF) break;
		alpha_size++;
		c = getc(fp);
		if (c != '\n' && c!= '\r' && c!= EOF) {
			printf("Error: %s is a bad alphabet file\n",path);
			exit(1);
		}
	}

	fclose(fp);

	if (alpha_size == 0) {
		printf("Error: %s is an empty alphabet file\n",path);
		exit(1);
	}
}

/*====================================*/
void set_in_alphabet(char *path)
/*====================================*/
{
	FILE *fp;
	char c; int i=0;

	if ((fp = fopen(path, "r"))==NULL) {
		printf("Error: cannot open alphabet file %s\n",path);
		exit(1);
	}
	
        c = getc(fp);
        while (c != EOF && c!='\n' && c!='\r') c = getc(fp);

	c = getc(fp);
	while (c != EOF) {
		while (c == '\n' || c=='\r') c = getc(fp);
		if (c == EOF) break;
		in_alphabet[i]=toupper(c); 
		i++;
		c = getc(fp);
	}

	fclose(fp);
}

/*====================================*/
void get_alphabet(char *path)
/*====================================*/
{
	count_alpha_size(path);
	in_alphabet=(char *)malloc((alpha_size)*sizeof(char));
	set_in_alphabet(path);
}

/*====================================*/
void clean_alphabet()
/*====================================*/
{
	free(in_alphabet);
}
