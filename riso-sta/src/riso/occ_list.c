#include<malloc.h>
#include<stdio.h>
#include<stdlib.h>
#include"occ_list.h"


#define ERR_MALLOC "error: malloc"

/* =========================================================== */
void quit_occ_list(char* str, int status)
/* =========================================================== */
{
    printf("%s\n",str);
    exit(status);
}

/* =========================================================== */
void add_node_occ(occ_list* l, struct node_occ* e)
/* =========================================================== */
{
    node_occ_elem *n = (node_occ_elem*) malloc(sizeof(struct node_occ_elem_struct));
    if (!n) quit_occ_list(ERR_MALLOC, 1);
    n->nocc = e;
    n->prox = *l;
    *l = n;
}

/* =========================================================== */
void free_node_occ_list(occ_list l)
/* =========================================================== */
{
    node_occ_elem *e;
    while (l) {
        e = l;
        l = l->prox;
        free(e->nocc); e->nocc=NULL;
        free(e); e=NULL;
    }
    l=NULL;
}
