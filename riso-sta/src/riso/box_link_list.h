#ifndef _BOX_LINK_LIST_H
#define _BOX_LINK_LIST_H

#include"../factor_tree/bit_tab.h"


struct box_link {
	unsigned int leaf; /* The master leaf index of the target leaf of the box-link */
	Bit_Tab *color;
};

typedef struct box_link_elem_struct {
    struct box_link* blink;
    struct box_link_elem_struct* prox;
} box_link_elem;

typedef struct box_link_elem_struct *box_link_list;

/* To call add_box_link just declare a box_link_list and initialize it to NULL */
void add_box_link(box_link_list* l, struct box_link* e);
void free_box_link_list(box_link_list l);

#endif
