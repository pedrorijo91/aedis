#ifndef _FACTOR_TREE_H
#define _FACTOR_TREE_H

#include"../factor_tree/bit_tab.h"
#include"box_link_list.h"


void build_gen_factor_tree();
void clean_gen_factor_tree();

int is_leaf(unsigned int node);
void init_box_colors(unsigned short box);

void init_su_tables();
void clean_su_tables();
void su_build_colors();
void su_clean_colors();
unsigned int su_get_first_child(unsigned int inode);
unsigned int su_get_sibling(unsigned int node);
unsigned int su_get_inode_depth(unsigned int inode);
unsigned int su_get_depth(unsigned int node);
unsigned int su_get_head(unsigned int node);
Bit_Tab *su_get_color(unsigned short box, unsigned int node);
unsigned int su_get_nb_colors(unsigned short box, unsigned int node);

#ifdef STATIC_BOX_LINKS
extern box_link_list **leaves_box_links;
void build_box_links();
void clean_box_links();
#endif /* #ifdef STATIC_BOX_LINKS */
void update_leaf_tree(unsigned short box, unsigned int cur_delta_k, unsigned int node);
void update_inode_tree(unsigned short box);
void free_tree(unsigned short box, unsigned int node);

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
void init_occurrences();
void reset_occurrences();
unsigned int get_occurrences(unsigned int leaf);
void clean_occurrences();
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */


#endif
