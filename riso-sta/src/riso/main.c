#include<stdio.h>
#include<stdlib.h>
#include"global_constants.h"
#include"parameters.h"
#include"factor_tree.h"
#include"extraction.h"
#include"../factor_tree/fasta.h"


#ifdef LINUX_CPU_TIME
#include"linux_cpu_time.h"
#endif /* #ifdef LINUX_CPU_TIME */

#ifndef LINUX_CPU_TIME
float print_cpu_time(char flag) {return (float)0;}
#endif /* #ifndef LINUX_CPU_TIME */

/*====================================*/
int main(int argc, char *argv[])
/*====================================*/
{
#ifndef LINUX_CPU_TIME
	unsigned int i;
#endif /* #ifndef LINUX_CPU_TIME */
	float t=0.0,t_factor_tree=0.0,t_colors=0.0,t_box_links=0.0,t_extraction=0.0;

	/* Get params */
	print_cpu_time(1);
	get_params(argv);
	t=print_cpu_time(0);

	/* Build generalized factor tree */
	print_cpu_time(1);
	build_gen_factor_tree();
	t_factor_tree=print_cpu_time(0);
	printf("%d-factor tree built in %.2f seconds\n",k,t_factor_tree);

	/* Build colors of the generalized factor tree */
	print_cpu_time(1);
	init_su_tables(); /* TODO: redoing Kurtz for speedup the extraction */
	su_build_colors();
	t_colors=print_cpu_time(0);
	printf("%d-factor tree colors built in %.2f seconds\n",k,t_colors);

	/* Backwards compatibility with SMILE output (for statistiscal significance purposes) */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	init_occurrences();
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifdef STATIC_BOX_LINKS */

	/* Build static box-links */
#ifdef STATIC_BOX_LINKS
	print_cpu_time(1);
	build_box_links();
	t_box_links=print_cpu_time(0);
	printf("box-links built in %.2f seconds\n",t_box_links);
#endif /* #ifdef STATIC_BOX_LINKS */

	/* Spell models */
	print_cpu_time(1);
	spell_models();
	t_extraction=print_cpu_time(0);
	printf("extraction done in %.2f seconds\n",t_extraction);
#ifndef SMILE_OUTPUT
	fprintf(output_file,"extraction time: %.2f seconds\n",t_extraction);
#endif /* #ifndef SMILE_OUTPUT */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	fprintf(output_file,"User time: %.2f sec.\n",t_extraction);
	put_SMILE_header();
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

	/* Cleanup everything and computing total time spent */
	print_cpu_time(1);
#ifdef STATIC_BOX_LINKS
	clean_box_links();
#endif /* #ifdef STATIC_BOX_LINKS */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	clean_occurrences();
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifdef STATIC_BOX_LINKS */
	su_clean_colors();
	clean_su_tables();
	clean_gen_factor_tree();
	clean_params();
	t=t+print_cpu_time(0);
    printf("total CPU time: %.2f\n",(double)(t+t_factor_tree+t_colors+t_box_links+t_extraction));
#ifndef LINUX_CPU_TIME
	scanf("%d",&i);
#endif /* #ifndef LINUX_CPU_TIME */
	exit(0);
}


