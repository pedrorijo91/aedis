#ifndef _GLOBAL_TYPES_H
#define _GLOBAL_TYPES_H


struct parameters {
	unsigned int q;			/* Quorum */
	unsigned short p;		/* Number of boxes */
	unsigned short *k_min;	/* Min size of each box */
	unsigned short *k_max;	/* Max size of each box */
	unsigned short *e;		/* Error per box */
	unsigned int *d_min;	/* Min distance between consecutive boxes */
	unsigned int *d_max;	/* Max distance between consecutive boxes */
};

#endif

