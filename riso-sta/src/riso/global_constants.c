#include<stdio.h>
#include"global_constants.h"

struct parameters params;

/* In/out variables */
char *in_alphabet;
FILE *output_file;
char *params_out_file;
unsigned int delta_k;
unsigned int *alpha_mask;
unsigned short alpha_size;

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
unsigned int extracted_k_max;
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */


