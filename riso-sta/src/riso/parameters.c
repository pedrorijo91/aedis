#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"global_constants.h"
#include"parameters.h"
#include"alphabet.h"
#include"../factor_tree/fasta.h"


/*====================================*/
void check_parameters(char *alphabet, char *fasta, char *output, double quorum)
/*====================================*/
{
	unsigned short i;

	printf("=============================================================\n");
	printf("Extraction is going to be made with the following parameters:\n");
	printf("=============================================================\n\n");
	printf("Alphabet file       %s\n",alphabet);
	printf("FASTA file          %s\n",fasta);
	printf("Output file         %s\n",output);
	printf("Quorum              %.0f%% (%d sequences in %d)\n",quorum,params.q,N);
	printf("Boxes               %d\n",params.p);
	if (params.p>=1) {
		for (i=0;i<params.p;i++) {
			printf("\nBOX %d\n",i+1);
			printf("Min length          %d\n",params.k_min[i]);
            printf("Max length          %d\n",params.k_max[i]);
			printf("Substitutions       %d\n",params.e[i]);
			if (params.p>1 && i<params.p-1) {
				printf("Min spacer length   %d\n",params.d_min[i]);
				printf("Max spacer length   %d\n",params.d_max[i]);
            }
		}
    }
	printf("\n          ------ CHECK THESE PARAMETERS! ------\n\n");

	params_out_file=NULL;

#ifdef CREATE_PARAMS_FILE
	{
		/* Creating parameters file */
		FILE *fpo; int length; char *out_file_name;
		length=strlen(output);
		params_out_file=(char*)malloc(sizeof(char)*(length+8));
		memset(params_out_file,0,sizeof(char)*(length+8));
#ifdef LINUX_PATH
		out_file_name=strrchr(output,'/');
#else /* #ifdef LINUX_PATH */
		out_file_name=strrchr(output,'\\');
#endif /* #ifdef LINUX_PATH */
		if (out_file_name) {
			length-=strlen(++out_file_name);
			strncpy(params_out_file,output,length);
		}
		strcat(params_out_file,"params-");
		strcat(params_out_file,out_file_name?out_file_name:output);
		if (fpo=fopen(params_out_file,"w")) {
			fprintf(fpo,"EXTRACTION CRITERIAS =======================================\n");
			fprintf(fpo,"Alphabet file       %s\n",alphabet);
			fprintf(fpo,"FASTA file          %s\n",fasta);
			fprintf(fpo,"Output file         %s\n",output);
			fprintf(fpo,"Quorum              %.0f\n",quorum);			
			fprintf(fpo,"Boxes               %d\n",params.p);
			if (params.p>1) {
				for (i=0;i<params.p;i++) {
					fprintf(fpo,"\n## Characteristics for box %d\n",i+1);
					fprintf(fpo,"BOX %d ======================\n",i+1);
					fprintf(fpo,"Min length          %d\n",params.k_min[i]);
					fprintf(fpo,"Max length          %d\n",params.k_max[i]);
					fprintf(fpo,"Substitutions       %d\n",params.e[i]);
					if (params.p>1 && i<params.p-1) {
						fprintf(fpo,"Min spacer length   %d\n",params.d_min[i]);
						fprintf(fpo,"Max spacer length   %d\n",params.d_max[i]);
					}
				}
			}
		}
		fclose(fpo);
	}
#endif /* #ifdef CREATE_PARAMS_FILE */
}

/*====================================*/
void get_params(char **argv)
/*====================================*/
{
	unsigned short i; int arg=1; double quorum=0.0;

	/* Reading alphabet file to initialize in_alphabet */
	get_alphabet(argv[arg++]);
	if (alpha_size>32) {
		printf("Error: alphabet too large, must be smaller than 32\n");
		exit(1);
	}
	alpha_mask=(unsigned int*)malloc(sizeof(unsigned int)*alpha_size);
	memset(alpha_mask,0,sizeof(unsigned int)*alpha_size);
	for (i=0;i<alpha_size;i++) alpha_mask[i]=(1<<i);

	/* Reading fasta file to initialize in_srt */
	get_fasta(argv[arg++]);

	/* Opening output file */ 
	if((output_file=fopen(argv[arg++],"w"))==NULL) {
		printf("Error: cannot open output file '%s'\n",argv[3]);
		exit(1);
    }

	/* Quorum */
	quorum=atof(argv[arg++]);
	if (quorum==0.0) params.q=(unsigned int)ceil((double)(70*N)/100.0);
	else params.q=(unsigned int)ceil((double)(quorum*N)/100.0);
	/* Number of boxes */
	params.p=atoi(argv[arg++]);
#ifdef STATIC_BOX_LINKS
	if (params.p>2) {
		printf("Error: static RISO is only for dyads\n");
		exit(1);
	}
#endif /* #ifdef STATIC_BOX_LINKS */
	/* Boxes parameteres */
	k=0;
	params.k_min=(unsigned short*)malloc(sizeof(unsigned short)*params.p);
	params.k_max=(unsigned short*)malloc(sizeof(unsigned short)*params.p);
	params.e=(unsigned short*)malloc(sizeof(unsigned short)*params.p);
	if (params.p>1) {
		params.d_min=(unsigned int*)malloc(sizeof(unsigned int)*(params.p-1));
		params.d_max=(unsigned int*)malloc(sizeof(unsigned int)*(params.p-1));
	}
	for (i=0;i<params.p;i++) {
		/* Min and max size per box */
		params.k_min[i]=atoi(argv[arg++]);
		params.k_max[i]=atoi(argv[arg++]);
		if (k<params.k_max[i]) k=params.k_max[i];
		/* Error per box */
		params.e[i]=atoi(argv[arg++]);
		if (i<params.p-1) {
			/* Min and max distance between consecutive boxes */
			params.d_min[i]=atoi(argv[arg++]);
			params.d_max[i]=atoi(argv[arg++]);
		}
	}
	delta_k=params.k_max[0]-params.k_min[0]+1;

	check_parameters(argv[1],argv[2],argv[3],quorum);
}

/*====================================*/
void clean_params()
/*====================================*/
{
	fclose(output_file);
	if (params_out_file) free(params_out_file);
	free(params.k_min);
	free(params.k_max);
	free(params.e);
	if (params.p>1) {
		free(params.d_min);
		free(params.d_max);
	}
	clean_fasta();
	clean_alphabet();
}

