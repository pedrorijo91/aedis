#include<sys/times.h>
#include<unistd.h>
#include<time.h>
#include"linux_cpu_time.h"

/*====================================*/
float print_cpu_time(char flag)
/*====================================*/
{
	float ust;
	struct tms tms;
	static float dust;

	times(&tms);
	ust =  (float) tms.tms_utime;
	if (flag) {
		dust = ust;
		return 0.0;
    } else {
		ust -= dust;
		return ust / sysconf(_SC_CLK_TCK);
    }
}
