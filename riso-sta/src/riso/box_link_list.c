#include<malloc.h>
#include<stdio.h>
#include<stdlib.h>
#include"box_link_list.h"


#define ERR_MALLOC "error: malloc"

/* =========================================================== */
void quit_box_link_list(char* str, int status)
/* =========================================================== */
{
    printf("%s\n",str);
    exit(status);
}

/* =========================================================== */
void add_box_link(box_link_list* l, struct box_link* e)
/* =========================================================== */
{
    box_link_elem *n = (box_link_elem*) malloc(sizeof(struct box_link_elem_struct));
    if (!n) quit_box_link_list(ERR_MALLOC, 1);
    n->blink = e;
    n->prox = *l;
    *l = n;
}

/* =========================================================== */
void free_box_link_list(box_link_list l)
/* =========================================================== */
{
    box_link_elem *e;
    while (l) {
        e = l;
        l = l->prox;
		free(e->blink->color); e->blink->color=NULL;
        free(e->blink); e->blink=NULL;
        free(e); e=NULL;
    }
    l=NULL;
}
