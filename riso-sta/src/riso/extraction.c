#include<stdio.h>
#include<assert.h>
#include"extraction.h"
#include"global_constants.h"
#include"factor_tree.h"
#include"../factor_tree/fasta.h"
#include"../factor_tree/bit_tab.h"
#include"occ_list.h"


char			*g_model; /* Model being considered by the recursion g_model[depth] */
unsigned int	g_cur_ext, /* Mask of the alphabet symbol being considered currently */
				g_cur_branches, /* Branches being considered currently */
				g_cur_ext_werr, /* Mask of the alphabet symbol being considered currently, when making an error */
				g_new_ext, /* Mask of the alphabet symbols for the next recursion step */
				g_new_nb_max_colors, /* Upperbound for the number of colors for the potential new occurrence */
				*g_new_forwards, /* Auxiliar variable to decide forwards on branches of the current step */
				*g_new_childs, /* Auxiliar variable to decide childs on branches of the current step */
				g_alphabet_mask, /* Constant which stores the mask of the whole alphabet */
				g_nb_models, /* Number of models already extracted */
				g_quorum; /* Quorum achieved by the model being considered currently */
occ_list		g_cur_occ_list, /* Occurrence being considered */
				g_new_occ_list; /* Occurrences for the next recursion step */
struct node_occ	*g_new_nocc; /* New occurrence being constructed for the next recursion step */

/* Disposable variables */
unsigned short	g_ushort;
unsigned int	g_uint;
char			g_char;

/* Statistic variables */
#ifdef DEBUG_STATISTICS
unsigned int	*g_nb_extensions,
				g_nb_attempted_models;
#endif /* #ifdef DEBUG_STATISTICS */

/* Backwards compatibility with SMILE output (for statistiscal significance purposes) */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
char			*g_numeric_model;
unsigned int	g_num_occ;
#endif /* #ifdef SMILE_OUT */
#endif /* STATIC_BOX_LINKS */


/*===========================================================*/
void start_output_file()
/*===========================================================*/
{
	unsigned short i;

#ifndef SMILE_OUTPUT
	if (params_out_file)
		fprintf(output_file,"extraction corresponding to %s file\n\n",params_out_file);
	fprintf(output_file,"model");
	for (i=0;i<params.p*(k+1)-5;i++)
		fprintf(output_file," ");
	fprintf(output_file,"\tquorum\n\n");
#endif /* #ifndef SMILE_OUTPUT */

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	for(i=0; i!=3; i++) {
		fprintf(output_file,"                                        ");
		fprintf(output_file,"                                       \n");
	}
	fprintf(output_file,"========================================");
	fprintf(output_file,"=======================================\n");
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
}

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
/*===========================================================*/
void put_SMILE_header()
/*===========================================================*/
{
        unsigned short i;
        unsigned int total_k_min=0, total_e=0;

        rewind(output_file);

        for (i=0;i<params.p;i++) {
                total_k_min+=params.k_min[i];
                total_e+=params.e[i];
        }
        fprintf(output_file,"%%%%%% %d %d/%d %d %d %d %d",
                params.p,params.q,N,num_leaves-N,total_k_min,extracted_k_max,total_e);
        if(params.p>1) {
                for(i=0;i<params.p;i++) {
                        fprintf(output_file," %d %d %d",params.k_min[i],params.k_max[i],params.e[i]);
                        if(i<params.p-1)
                                fprintf(output_file," %d %d",params.d_min[i],params.d_max[i]);
                }
        }
        fprintf(output_file," alphabet ");
        for (i=0;i<alpha_size;i++)
                fprintf(output_file,"%c",in_alphabet[i]);
        fprintf(output_file,"$");
}
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

/*===========================================================*/
void finish_output_file()
/*===========================================================*/
{
#ifndef SMILE_OUTPUT
	fprintf(output_file,"\nnumber of models: %d\n",g_nb_models);
#endif /* #ifndef SMILE_OUTPUT */

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	fprintf(output_file,"Nb models: %d\n",g_nb_models);
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
}

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
/*===========================================================*/
void get_num_occ()
/*===========================================================*/
{
	g_num_occ=0;

	while (g_cur_occ_list) {
		g_num_occ+=get_occurrences(g_cur_occ_list->nocc->child);
		g_cur_occ_list=g_cur_occ_list->prox;
	}
}
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

/*===========================================================*/
void keep_model()
/*===========================================================*/
{
    g_nb_models++;
#ifndef SMILE_OUTPUT
	fprintf(output_file,"%s %d\n",g_model,g_quorum);
#endif /* #ifndef SMILE_OUTPUT */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
    fprintf(output_file,"%s %s %d\t%d\n",g_model,g_numeric_model,g_quorum,g_num_occ);
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
}

/*===========================================================*/
unsigned int get_branches(unsigned short box)
/*===========================================================*/
{
    assert((!g_new_occ_list->nocc->forward && !g_new_occ_list->nocc->depth) || 
		g_new_occ_list->nocc->forward<g_new_occ_list->nocc->depth);
    
    if (is_leaf(g_new_occ_list->nocc->node)) return 0;

    if (g_new_occ_list->nocc->forward) {
        g_char = in_str[su_get_head(g_new_occ_list->nocc->child)+su_get_inode_depth(g_new_occ_list->nocc->node)+g_new_occ_list->nocc->forward];
		for (g_ushort=0;g_ushort<alpha_size;g_ushort++) {
			if (g_char==in_alphabet[g_ushort]) {
				return alpha_mask[g_ushort];
			}
		}
		return 0;
    } else {
        unsigned int child, node_depth = su_get_inode_depth(g_new_occ_list->nocc->node);
		g_uint=0; g_ushort=0;
        for (child=su_get_first_child(g_new_occ_list->nocc->node);child;child=su_get_sibling(child)) {
			if (su_get_nb_colors(box,child)) {
				g_char = in_str[su_get_head(child)+node_depth];
				for (;g_ushort<alpha_size;g_ushort++) {
					if (g_char==in_alphabet[g_ushort]) {
						g_uint|=alpha_mask[g_ushort];
						g_ushort++;
						break;
					}
				}
			}
        }
		return g_uint;
    }
}

/*===========================================================*/
unsigned int get_branches_and_childs(unsigned short box)
/*===========================================================*/
{
    assert((!g_cur_occ_list->nocc->depth && !g_cur_occ_list->nocc->forward) || 
		g_cur_occ_list->nocc->forward<g_cur_occ_list->nocc->depth);
    
    if (is_leaf(g_cur_occ_list->nocc->node)) return 0;

    if (g_cur_occ_list->nocc->forward) {
        g_char = in_str[su_get_head(g_cur_occ_list->nocc->child)+su_get_inode_depth(g_cur_occ_list->nocc->node)+g_cur_occ_list->nocc->forward];
		for (g_ushort=0;g_ushort<alpha_size;g_ushort++) {
			if (g_char==in_alphabet[g_ushort]) {
				return alpha_mask[g_ushort];
			}
		}
		return 0;
    } else {
        unsigned int child, node_depth;    
        memset(g_new_forwards,0,sizeof(unsigned int)*alpha_size);
        memset(g_new_childs,0,sizeof(int)*alpha_size);
        node_depth = su_get_inode_depth(g_cur_occ_list->nocc->node);
		g_uint=0; g_ushort=0;
        for (child=su_get_first_child(g_cur_occ_list->nocc->node);child;child=su_get_sibling(child)) {
			if (su_get_nb_colors(box,child)) {
	            g_char = in_str[su_get_head(child)+node_depth];
				for (;g_ushort<alpha_size;g_ushort++) {
					if (g_char==in_alphabet[g_ushort]) {
						g_uint|=alpha_mask[g_ushort];
						g_new_childs[g_ushort]=child;
						g_new_forwards[g_ushort]=su_get_depth(child)-node_depth;
						g_ushort++;
						break;
					}
				}
			}
        }
		return g_uint;
    }
}

/*===========================================================*/
occ_list get_initial_occurrences()
/*===========================================================*/
{
	/* At the beginning the root is the only node occurrence */
	occ_list occ=NULL;
	struct node_occ *nocc=(struct node_occ*)malloc(sizeof(struct node_occ));
    nocc->node=0/*root*/;
    nocc->err=0;
    nocc->depth=0;
    nocc->forward=0;
    nocc->child=0/*root*/;
    add_node_occ(&occ,nocc);
	return occ;
}

/*===========================================================*/
unsigned int get_initial_branches(unsigned short box)
/*===========================================================*/
{
	/* At the beggining if errors are allowed consider the whole alphabet,
		otherwise consider only the branches leaving the root */

	if (params.e[box]) return g_alphabet_mask;
	else {
		unsigned int child;
		g_uint=0; g_ushort=0;
        for (child=su_get_first_child(0);child;child=su_get_sibling(child)) {
			if (su_get_nb_colors(box,child)) {
				g_char = in_str[su_get_head(child)];
				for (;g_ushort<alpha_size;g_ushort++) {
					if (g_char==in_alphabet[g_ushort]) {
						g_uint|=alpha_mask[g_ushort];
						g_ushort++;
						break;
					}
				}
			}
        }
		return g_uint;
	}
}

/*===========================================================*/
void update_tree(unsigned short box, unsigned int cur_delta_k)
/*===========================================================*/
{
	init_box_colors(box);
	while (g_cur_occ_list) {
		update_leaf_tree(box,cur_delta_k,g_cur_occ_list->nocc->child/* TODO: g_cur_occ_list->nocc->node? podemos usar de factor o child? */);
		g_cur_occ_list=g_cur_occ_list->prox;
	}
	update_inode_tree(box);
}

/*===========================================================*/
void spell_models_rec(unsigned short box, unsigned short acc_depth, unsigned short depth, occ_list occ, unsigned int ext)
/*===========================================================*/
{
	unsigned short i;


	if (depth>=params.k_min[box]) {
		if (box==params.p-1) { /* Case of last box of the structured model */
			g_model[acc_depth+depth]='\0';
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
			g_uint=acc_depth+depth-(params.p-1);
			if (extracted_k_max<g_uint) extracted_k_max=g_uint;
			g_numeric_model[acc_depth+depth]='\0';
			g_cur_occ_list=occ;
			get_num_occ();
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
			keep_model();
		} else { /* Case of an intermediate box of the structured model */
			g_model[acc_depth+depth]='_';
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
			g_numeric_model[acc_depth+depth]='-';
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
			g_cur_occ_list=occ;
			update_tree((unsigned short)(box+1),depth-params.k_min[box]);
			spell_models_rec((unsigned short)(box+1),(unsigned short)(acc_depth+depth+1),0,get_initial_occurrences(),get_initial_branches((unsigned short)(box+1)));
			free_tree((unsigned short)(box+1),0);
		}
		if (depth==params.k_max[box]) {
			free_node_occ_list(occ);
			return;
		}
	}
    
    /* For each symbol alpha in ext_model */
    for (i=0;i<alpha_size;i++) {
        
		g_new_occ_list=NULL; g_cur_occ_list=occ;
        g_new_ext=g_cur_branches=g_new_nb_max_colors=0;
        g_cur_ext=ext&(1<<i);

#ifdef DEBUG_STATISTICS
		g_nb_extensions[box]++;
#endif /* #ifdef DEBUG_STATISTICS */

        /* For each pair (x,err) in occ_model */
        while (g_cur_occ_list) {

            g_cur_branches=get_branches_and_childs(box);

            /* If there is a branch leaving node x with a label starting with alpha */
            if (g_cur_ext&g_cur_branches) {
				g_new_nocc=(struct node_occ*)malloc(sizeof(struct node_occ));
				if (!g_cur_occ_list->nocc->forward) {
					if (g_new_forwards[i]==1) { /* One label only between node-child */
						g_new_nocc->node=g_new_childs[i];
						g_new_nocc->err=g_cur_occ_list->nocc->err;
						g_new_nocc->forward=0;
						g_new_nocc->depth=0;
						g_new_nocc->child=g_new_childs[i]; /* When no forward child=node */
					} else { /* More than one label between node-child */
						g_new_nocc->node=g_cur_occ_list->nocc->node;
						g_new_nocc->err=g_cur_occ_list->nocc->err;
						g_new_nocc->forward=(g_cur_occ_list->nocc->forward)+1; /* We already treated one label between node-child */
						g_new_nocc->depth=g_new_forwards[i];
						g_new_nocc->child=g_new_childs[i];                            
					}
				} else if (g_cur_occ_list->nocc->depth-(g_cur_occ_list->nocc->forward+1)==0) { /* Proceed with child */
					g_new_nocc->node=g_cur_occ_list->nocc->child;
					g_new_nocc->err=g_cur_occ_list->nocc->err;
					g_new_nocc->forward=0;
					g_new_nocc->depth=0;
					g_new_nocc->child=g_cur_occ_list->nocc->child; /* When no forward child=node */
				} else { /* Continue with node */
					g_new_nocc->node=g_cur_occ_list->nocc->node;
					g_new_nocc->err=g_cur_occ_list->nocc->err;
					g_new_nocc->forward=(g_cur_occ_list->nocc->forward)+1;
					g_new_nocc->depth=g_cur_occ_list->nocc->depth;
					g_new_nocc->child=g_cur_occ_list->nocc->child;
				}
				add_node_occ(&g_new_occ_list,g_new_nocc);
				g_new_nb_max_colors+=su_get_nb_colors(box,g_new_nocc->child);
				if (g_new_ext!=g_alphabet_mask)
					g_new_ext = ((g_cur_occ_list->nocc->err==params.e[box]) ?
						(g_new_ext|get_branches(box)) : g_alphabet_mask);
            }
            
            /* For each branch leaving x except the one labeled with alpha */
            if (g_cur_occ_list->nocc->err<params.e[box]) {
                unsigned short j;
                for (j=0;j<alpha_size;j++) {
                    g_cur_ext_werr = g_cur_branches&(1<<j);
                    if (g_cur_ext_werr&~g_cur_ext) {
						g_new_nocc=(struct node_occ*) malloc(sizeof(struct node_occ));
						if (!g_cur_occ_list->nocc->forward) {
							if (g_new_forwards[j]==1) { /* One label only between node-child */
								g_new_nocc->node=g_new_childs[j];
								g_new_nocc->err=g_cur_occ_list->nocc->err+1;
								g_new_nocc->forward=0;
								g_new_nocc->depth=0;
								g_new_nocc->child=g_new_childs[j]; /* When no forward child=node */
							} else { /* More than one label between node-child */
								g_new_nocc->node=g_cur_occ_list->nocc->node;
								g_new_nocc->err=g_cur_occ_list->nocc->err+1;
								g_new_nocc->forward=(g_cur_occ_list->nocc->forward)+1; /* We already treated one label between node-child */
								g_new_nocc->depth=g_new_forwards[j];
								g_new_nocc->child=g_new_childs[j];                            
							}
						} else if (g_cur_occ_list->nocc->depth-(g_cur_occ_list->nocc->forward+1)==0) { /* Proceed with child */
							g_new_nocc->node=g_cur_occ_list->nocc->child;
							g_new_nocc->err=g_cur_occ_list->nocc->err+1;
							g_new_nocc->forward=0;
							g_new_nocc->depth=0;
							g_new_nocc->child=g_cur_occ_list->nocc->child; /* When no forward child=node */
						} else { /* Continue with node */
							g_new_nocc->node=g_cur_occ_list->nocc->node;
							g_new_nocc->err=g_cur_occ_list->nocc->err+1;
							g_new_nocc->forward=(g_cur_occ_list->nocc->forward)+1;
							g_new_nocc->depth=g_cur_occ_list->nocc->depth;
							g_new_nocc->child=g_cur_occ_list->nocc->child;
						}
						add_node_occ(&g_new_occ_list,g_new_nocc);
						g_new_nb_max_colors+=su_get_nb_colors(box,g_new_nocc->child);
						if (g_new_ext!=g_alphabet_mask)
							g_new_ext = ((g_cur_occ_list->nocc->err==params.e[box]) ?
								(g_new_ext|get_branches(box)) : g_alphabet_mask);
                    }
                }
            }
			g_cur_occ_list = g_cur_occ_list->prox;
        }/*while*/

		if (g_new_nb_max_colors>=params.q) {
			Bit_Tab *new_color=AllocBitTab();
			g_cur_occ_list=g_new_occ_list;
			while (g_cur_occ_list) {
				if (g_cur_occ_list->nocc->forward)
					fusionneBitTab(&new_color,su_get_color(box,g_cur_occ_list->nocc->child));
				else
					fusionneBitTab(&new_color,su_get_color(box,g_cur_occ_list->nocc->node));
				g_cur_occ_list = g_cur_occ_list->prox;
			}
			if ((g_quorum=(unsigned int)nbSequenceInBitTab(new_color))>=params.q) {
				free(new_color);
				g_model[acc_depth+depth]=in_alphabet[i];
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
				g_numeric_model[acc_depth+depth]='0'+i;
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
				spell_models_rec(box,acc_depth,(unsigned short)(depth+1),g_new_occ_list,g_new_ext);
			} else {
#ifdef DEBUG_STATISTICS
				g_nb_attempted_models++;
#endif /* #ifdef DEBUG_STATISTICS */
				free(new_color);
				free_node_occ_list(g_new_occ_list);
			}
		} else {
#ifdef DEBUG_STATISTICS
			g_nb_attempted_models++;
#endif /* #ifdef DEBUG_STATISTICS */
			free_node_occ_list(g_new_occ_list);
		}
    }/*for*/

    free_node_occ_list(occ);
}

/*===========================================================*/
void spell_models()
/*===========================================================*/
{
	/* Initialize global variables */
	g_nb_models=0;
	g_model=(char*)malloc(sizeof(char)*(params.p*(k+1))); /* Model of the p boxes separated by '_' and ended with '\n' */
	memset((void *)g_model,0,sizeof(char)*(params.p*(k+1)));
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	g_numeric_model=(char*)malloc(sizeof(char)*(params.p*(k+1))); /* Numeric model of the p boxes separated by '-' and ended with '\n' */
	memset((void *)g_numeric_model,0,sizeof(char)*(params.p*(k+1)));
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */
	for (g_alphabet_mask=0,g_ushort=0;g_ushort<alpha_size;g_ushort++) 
		g_alphabet_mask|=alpha_mask[g_ushort];
    g_new_forwards=(int*)malloc(sizeof(int)*alpha_size);    
    g_new_childs=(unsigned int*)malloc(sizeof(unsigned int)*alpha_size);
#ifdef DEBUG_STATISTICS
	g_nb_extensions=(unsigned int*)malloc(sizeof(unsigned int)*params.p);
	memset((void*)g_nb_extensions,0,sizeof(unsigned int)*params.p);
#endif /* #ifdef DEBUG_STATISTICS */

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	extracted_k_max=0;
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

	start_output_file();

	/* Spell single models */
    spell_models_rec(
		0, /* first box being extracted */
		0, /* previous boxes sizes accumulated */
		0, /* depth already treated */
		get_initial_occurrences(), /* list os node occurrences */
		get_initial_branches(0)); /* possible extensions from the node being considered */

	finish_output_file();

#ifdef DEBUG_STATISTICS
	for (g_ushort=0;g_ushort<params.p;g_ushort++)
		printf("\t number of extensions in box %d is %d\n", g_ushort+1, g_nb_extensions[g_ushort]);
	free(g_nb_extensions);
	/* The number of models we found out that are not to be reported (only by quorum) */
	printf("\t number of aborted models is %d (in %d tried)\n", g_nb_attempted_models,g_nb_attempted_models+g_nb_models);
#endif /* #ifdef DEBUG_STATISTICS */

	/* Cleanup global variables */
    free(g_new_forwards);
    free(g_new_childs);
	free(g_model);
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
	free(g_numeric_model);
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

	printf("number of models: %d\n",g_nb_models);
}

