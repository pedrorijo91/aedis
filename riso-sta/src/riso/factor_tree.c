#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"factor_tree.h"
#include"global_constants.h"
#include"../factor_tree/fasta.h"
#include"../factor_tree/bit_tab.h"
#include"leaf_list.h"

#define LNODE_SIZE 4
#define SNODE_SIZE 2

#define LEAF_BIT 0x10000000
#define SIBLING_BIT 0x20000000
#define MASTER_LEAF_BIT 0x80000000
#define SMALL_INODE_MASK 0xF8000000


/* Kurtz's variables */
unsigned int	*leaves, /* Table of leaves */
/* Table of leaves' master: 
	 During construction of the factor tree:
	   - to each leaf stores its master whenerver leaf is not a master,
	   - to each master stores the depth of the master 
	 Before constructing colors of the factor tree:
	   - to each leaf stores its master whenerver leaf is not a master,
	   - to each master stores the index of master creation */
				*leaves_master, 
				**inodes; /* Table of inodes (large and small) */
int				last_inode_created,
				last_leaf_created,
				master_leaves_created,
				small_inodes_created; /* until enlarge */
#ifdef DEBUG_STATISTICS
int				large_inodes_created;
#endif

/* Ukkonen's variables */
int				branching, 
				undef_sl_father_inode;
unsigned int	phase,
				father_inode, 
				grand_father_inode, 
				prev_step_inode, 
				last_inode_reached, 
				new_inode_created,
				last_large_inode_created,
				depth_in_phase[31],
				head_in_phase[31];

/* Factor tree variables */
int				was_leaf_created;
unsigned int	first_in_queue,
				last_in_queue,
				last_position_reached,
				last_leaf_reached,
				father_last_leaf_reached;

/* Generalized factor tree variables */
unsigned int	cur_str;

/* Colors and multiple extraction variables */
Bit_Tab			***leaves_color, /* For p boxes because of the extraction */
				***inodes_color; /* For p boxes because of the extraction */
unsigned int	*masters_depth;
unsigned short	**nb_leaves_color, /* LIMITATION: This unsigned short is limiting N to 2^16 */
								   /* For p boxes because of the extraction */
				**nb_inodes_color; /* LIMITATION: This unsigned short is limiting N to 2^16 */
								   /* For p boxes because of the extraction */
leaf_list		**reached_leaves; /* For p-2 boxes for extractions with p>2 */

/* Speedup test variables */
unsigned int	*su_inode_first_child,
				*su_inode_sibling,
				*su_inode_head,
				*su_leaf_sibling,
				*su_leaf_master_index,
				*su_leaf_next_leaf,
				*su_leaf_in_str,
#ifdef DEBUG_STATISTICS
				g_nb_dispoded_leaves,
#endif /* #ifdef DEBUG_STATISTICS */
				g_disposable_distance; /* Minimum distance a leaf must be from the end of its input string in order to spell a model */

unsigned short	*su_inode_depth, /* LIMITATION: This unsigned short is limiting N to 2^16 */
				*su_leaf_depth; /* LIMITATION: This unsigned short is limiting N to 2^16 */

/* Box-links variables */
#ifdef STATIC_BOX_LINKS
box_link_list	**leaves_box_links;
#endif /* #ifded STATIC_BOX_LINKS */

/* Backwards compatibility with SMILE output (for statistiscal significance purposes) */
#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
unsigned int	*nb_leaves_occ;
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */


/****************************************************************/
/****************************************************************/
/*				ILLI CODING (Kurtz + Allali)					*/				
/****************************************************************/
/****************************************************************/


/*====================================*/
int is_leaf(unsigned int node)
/*====================================*/
{
	return LEAF_BIT & node;
}

/*====================================*/
int has_sibling(unsigned int node /* Scheme B */) 
/*====================================*/
{
	return SIBLING_BIT & node;
}

/*====================================*/
void set_suffix_link_on_rightmost_child(unsigned int first_child, unsigned int suffix_link)
/*====================================*/
{
	if(is_leaf(first_child)){
		int first_child_index=first_child&(~LEAF_BIT);
		if(! (leaves[first_child_index] & MASTER_LEAF_BIT) ) 
			first_child_index=leaves_master[first_child_index];
		if(has_sibling(leaves[first_child_index]))
			set_suffix_link_on_rightmost_child((leaves[first_child_index] << 3) >> 3,suffix_link);
			/* Eventual optimization with &0x1FFFFFFF */
		else
			leaves[first_child_index]|=suffix_link;
	} else {
		if(has_sibling(inodes[first_child][1]))
			set_suffix_link_on_rightmost_child((inodes[first_child][1] << 3) >> 3,suffix_link);
			/* Eventual optimization with &0x1FFFFFFF */
		else
			inodes[first_child][1]|=suffix_link;
	}
}


/*====================================*/
void create_small_inode(unsigned int first_child, unsigned int sibling)
/*====================================*/
{
	unsigned int *cur_inode;

	last_inode_created++;
	small_inodes_created++;
	cur_inode=(inodes[last_inode_created]=(int *)malloc(SNODE_SIZE*sizeof(int)));
	memset((void *) cur_inode,0,SNODE_SIZE*sizeof(int));
	
	/* Coding A */
	/* The first 5 bits of A are equal to the distance until the large node 
	     with which it is associated initially set to 31 (only reset in enlarge_inode) */
	cur_inode[0]=SMALL_INODE_MASK;
	/* The first 27 bits of first_child are placed on the remaining 27 bits of A */
	cur_inode[0]|=first_child >> 2;

	/* Coding B */
	/* The last 2 bits of first_child are placed on the first 2 bits of B */
	cur_inode[1]=first_child << 30;
	/* If there is a sibling then put sibling bit on third position of B and the 
	     sibling on the remaining bits of B */
	if(sibling){
		cur_inode[1]|=SIBLING_BIT;
		cur_inode[1]|=sibling;
	} 
}

/*====================================*/
unsigned int get_first_child(unsigned int inode)
/*====================================*/
{
	unsigned int *cur_inode=inodes[inode], res=0;

	res=((cur_inode[0]&0x7FFFFFF) << 2);
	res|=(cur_inode[1] >> 30);

	return res;
}

/*====================================*/
int is_small_inode(unsigned int inode)
/*====================================*/
{
	return (inodes[inode][0] & SMALL_INODE_MASK); 
}

/*====================================*/
unsigned int get_inode_depth(unsigned int inode)
/*====================================*/
{
	unsigned int *cur_inode=inodes[inode];

	if(is_small_inode(inode)){
		if (inode>last_large_inode_created) {
			return depth_in_phase[inode-last_large_inode_created-1];
		} else {
			int dist=(cur_inode[0] >> 27);
			return get_inode_depth(inode+dist)+dist;
		}
	} else {
		/* If inode <= 2^26 we use C1 + C2 scheme without using 2 bits of the leaf */
		if(inode <= 0x4000000){
			/* If the first bit of C is flagged we have the C1 scheme */
			if(cur_inode[2] & 0x80000000) {
				/* The depth is coded in the last 10 bits of C1 */
				return (cur_inode[2] & 0x3FF);
			} else {
				/* The depth is coded in the last 27 bits of C2 */
				return cur_inode[2];
			}
		} else { 
			/* Else the inode > 2^26 we use C1' + C2 scheme without the 2 bits of the leaf */
			/* If the first bit of C is flagged we have the C1' scheme */
			if(cur_inode[2] & 0x80000000) {
				/* The depth is coded in the last 8 bits of C1' */
				return (cur_inode[2] & 0xFF);
			} else {
				/* The depth is coded in the last 27 bits of C2 */
				return cur_inode[2];
			}
		}
	}
}

/*====================================*/
void set_suffix_link(unsigned int inode, unsigned int suffix_link)
/*====================================*/
{
	unsigned int depth=get_inode_depth(inode);
	unsigned int *cur_inode=inodes[inode];

	/* Coding C and D */
	/* If inode <= 2^26 we use C1 + C2 scheme without using 2 bits of the leaf */
	if(inode <= 0x4000000){
		/* If depth < 2^10 we use C1 + D */
		if(depth < 0x400) {
			/* The suffix_link is coded on the 2..22 bits of C1 and first 5 bits of D */
			cur_inode[2]|=((suffix_link >> 5) << 10); /* Eventual optimization with (&= 0xFFFFFE0)<<5 */
			cur_inode[3]|=(suffix_link << 27);
			
		} else {
			/* The suffix_link is coded on rightmost child (B scheme) whose field 
			     right sibling is nil */
			set_suffix_link_on_rightmost_child(get_first_child(inode),suffix_link);
		}
	} else { 
		/* Else the last_inode_created > 2^26 we use C1' + C2 scheme without the 2 bits of the leaf */
		/* If depth < 2^8 we use C1' + D */
		if(depth < 0x100) {
			/* The suffix_link is coded on the 2..24 bits of C1' and first 5 bits of D */
			cur_inode[2]|=((suffix_link >> 5) << 8); /* Eventual optimization */
			cur_inode[3]|=(suffix_link << 27);						
		} else {
			/* The suffix_link is coded on rightmost child (B scheme) whose 
			     field right sibling is nil */
			set_suffix_link_on_rightmost_child(get_first_child(inode),suffix_link);
		}
	}
}

/*====================================*/
void enlarge_inode(unsigned int depth, unsigned int head)
/*====================================*/
{
	unsigned int *cur_inode;
	int i;

	inodes[last_inode_created]=(unsigned int *)realloc((void *)inodes[last_inode_created],LNODE_SIZE*sizeof(int));
	cur_inode=inodes[last_inode_created];
	cur_inode[2]=(cur_inode[3]=0);

	/* Coding C and D */
	/* If last_inode_created <= 2^26 we use C1 + C2 scheme without using 2 bits of the leaf */
	if(last_inode_created <= 0x4000000){
		/* If depth < 2^10 we use C1 + D */
		if(depth < 0x400) {
			/* Flag the first bit of C to identify it as C1 scheme */
			cur_inode[2]=0x80000000;
			/* The depth is coded in the last 10 bits of C1 */
			cur_inode[2]|=depth;
			/* The head position is coded on the remaining 27 bits of D */
			cur_inode[3]=head;
		} else {
			/* Do not flag the first bit of C to identify it as C2 scheme */
			/* The depth is coded in the last 27 bits of C2 */
			cur_inode[2]=depth;
			/* The head position is coded on the last 27 bits of D */
			cur_inode[3]=head;
		}
	} else { 
		/* Else the last_inode_created > 2^26 we use C1' + C2 scheme without the 2 bits of the leaf */
		/* If depth < 2^8 we use C1' + D */
		if(depth < 0x100) {
			/* Flag the first bit of C to identify it as C1' scheme */
			cur_inode[2]=0x80000000;
			/* The depth is coded in the last 8 bits of C1' */
			cur_inode[2]|=depth;
			/* The head position is coded on the remaining 27 bits of D */
			cur_inode[3]=head;
		} else {
			/* Do not flag the first bit of C to identify it as C2 scheme */
			/* The depth is coded in the last 27 bits of C2 */
			cur_inode[2]=depth;
			/* The head position is coded on the last 27 bits of D */
			cur_inode[3]=head;
		}
	}

	/* Erase small inode flag */
	inodes[last_inode_created][0]&=~SMALL_INODE_MASK;
	/* Set distance to small nodes with which it is associated */
	for(i=1;i<small_inodes_created;i++){
		/* Erase small inode flag */
		inodes[last_inode_created-i][0]&=~SMALL_INODE_MASK;
		/* The first 5 bits of A are equal to the distance to the large node 
		     with which it is associated */
		inodes[last_inode_created-i][0]|= (i << 27);
	}
	small_inodes_created=0;
	last_large_inode_created=last_inode_created;

#ifdef DEBUG_STATISTICS
	large_inodes_created++;
#endif
}

/*====================================*/
void create_master_leaf(unsigned int sibling)
/*====================================*/
{
	unsigned int depth;

	last_leaf_created++;
	master_leaves_created++;

	/* Coding B1 */
	/* The first bit of B1 is flagged to indentify it as B1 */
	leaves[last_leaf_created]=MASTER_LEAF_BIT;
	/* The second bit of B1 is wasted */
	/* If there is a sibling then put sibling bit on third position of B1 and the */
	/* sibling on the remaining bits of B1 */
	if(sibling){
		leaves[last_leaf_created]|=SIBLING_BIT;
		leaves[last_leaf_created]|=sibling;
	} /* Else the remaining bits are eventualy used by put_suffix_link_on_rightmost_child */
	
	/* The master leaf of the master is the master itself, so since this information
		 is useless we store the depth of the master leaf instead */
	depth=offset[cur_str]-last_leaf_created;
	leaves_master[last_leaf_created]=((depth>k) ? k : depth);
}

/*====================================*/
void create_slave_leaf(unsigned int next_leaf)
/*====================================*/
{
	unsigned int next_leaf_index;

	last_leaf_created++;

	/* Coding B2 */
	/* The first bit of B2 is not flagged to indentify it as B2 */
	/* The next 2 bits of B2 are wasted */
	/* The last 29 bits code the next leaf in the list of leaf occurrences */
	leaves[last_leaf_created]=next_leaf;
	
	/* Set the master leaf of the last leaf */
	next_leaf_index=next_leaf&~LEAF_BIT;
	leaves_master[last_leaf_created]=((leaves[next_leaf_index]&MASTER_LEAF_BIT) ? 
		next_leaf_index : leaves_master[next_leaf_index]);
}


/****************************************************************/
/****************************************************************/
/*				SUFFIX TREE (Ukkonen's algorithm)				*/
/****************************************************************/
/****************************************************************/


/*====================================*/
unsigned int get_inode_head(unsigned int inode)
/*====================================*/
{
	unsigned int *cur_inode=inodes[inode];

	if(is_small_inode(inode)){
		if (inode>last_large_inode_created){
			return head_in_phase[inode-last_large_inode_created-1];
		} else {
			int dist=(cur_inode[0] >> 27);
			return get_inode_head(inode+dist)-dist;
		}
	} else {
		/* The head is coded in the last 27 bits of D */
		return (cur_inode[3] << 5) >> 5; /* Eventual optimization */
	}
}

/*====================================*/
unsigned int get_head(unsigned int node)
/*====================================*/
{
	if(is_leaf(node)) return (node & ~LEAF_BIT);
	else return get_inode_head(node);
}

/*====================================*/
unsigned int get_sibling(unsigned int node)
/*====================================*/
{
	if(is_leaf(node)){
		node&=(~LEAF_BIT);
		if(! (leaves[node] & MASTER_LEAF_BIT) ) 
			node=leaves_master[node];
		if (has_sibling(leaves[node]))
			return (leaves[node] << 3) >> 3;
			/* Eventual optimization with &0x1FFFFFFF */
		else
			return 0;
	} else {
		if (has_sibling(inodes[node][1]))
			return (inodes[node][1] << 3) >> 3;
		else
			return 0;
			/* Eventual optimization with &0x1FFFFFFF */
	}
}

/*====================================*/
unsigned int get_child(unsigned int inode, char c)
/*====================================*/
{
	unsigned int child=get_first_child(inode), inode_depth=get_inode_depth(inode);

	while(in_str[get_head(child)+inode_depth]!=c && child){
		child=get_sibling(child);
	}
	return child;
}

/*====================================*/
unsigned int get_leaf_depth_in_construction(unsigned int leaf)
/*====================================*/
{
	unsigned int leaf_index=(leaf&~LEAF_BIT);

	if(! (leaves[leaf_index] & MASTER_LEAF_BIT) ) 
		leaf_index=leaves_master[leaf_index];

	if(leaf_index<offset[cur_str-1]) 
		return leaves_master[leaf_index];
	else {
		unsigned int depth = phase-leaf_index;
		return k < depth ? k : depth;
	}
}

/*====================================*/
unsigned int get_depth_in_construction(unsigned int node)
/*====================================*/
{
	if(is_leaf(node)) return get_leaf_depth_in_construction(node);
	else return get_inode_depth(node);
}

/*====================================*/
void add_child(unsigned int inode, char c)
/*====================================*/
{
	unsigned int child=get_first_child(inode), prev_child=0,
		inode_depth=get_inode_depth(inode);

	while(in_str[get_head(child)+inode_depth]<c && child){
		prev_child=child;
		child=get_sibling(child);
	}

	if(!prev_child){/* New first child */
		create_master_leaf(child);
		/* assert(in_str[last_leaf_created+inode_depth]==c); */
		/* Delete first child */
		inodes[inode][0]&=0xF8000000;
		inodes[inode][1]&=0x3FFFFFFF;
		/* Set first child */
		inodes[inode][0]|=((LEAF_BIT|last_leaf_created) >> 2);
		inodes[inode][1]|=((LEAF_BIT|last_leaf_created) << 30);
	} else if(!child){ /* New last child */
		create_master_leaf(0);
		/* assert(in_str[last_leaf_created+inode_depth]==c); */
		if(is_leaf(prev_child)){
			int prev_child_index= prev_child & (~LEAF_BIT);
			if(! (leaves[prev_child_index] & MASTER_LEAF_BIT) ) 
				prev_child_index=leaves_master[prev_child_index];
			/* Copy suffix link from prev_child to last_leaf_created */
			leaves[last_leaf_created]|=((leaves[prev_child_index] << 3) >> 3); 
			leaves[prev_child_index]&=0xE0000000; /* Delete possible suffix link */
			leaves[prev_child_index]|=SIBLING_BIT;/* Set sibling flag */
			leaves[prev_child_index]|=(LEAF_BIT|last_leaf_created); /* Set sibling */
		} else {
			/* Copy suffix link from prev_child to last_leaf_created */
			leaves[last_leaf_created]|=((inodes[prev_child][1] << 3) >> 3); 
			inodes[prev_child][1]&=0xE0000000; /* Delete possible suffix link */
			inodes[prev_child][1]|=SIBLING_BIT;/* Set sibling flag */
			inodes[prev_child][1]|=(LEAF_BIT|last_leaf_created); /* Set sibling */
		}
	} else {/* New in between child */
		create_master_leaf(child);
		/* assert(in_str[last_leaf_created+inode_depth]==c); */
		if(is_leaf(prev_child)){
			int prev_child_index= prev_child & (~LEAF_BIT);
			if(! (leaves[prev_child_index] & MASTER_LEAF_BIT) ) 
				prev_child_index=leaves_master[prev_child_index];
			leaves[prev_child_index]&=0xE0000000; /* Delete sibling */
			leaves[prev_child_index]|=(LEAF_BIT|last_leaf_created); /* Set sibling */
		} else {
			inodes[prev_child][1]&=0xE0000000; /* Delete sibling */
			inodes[prev_child][1]|=(LEAF_BIT|last_leaf_created); /* Set sibling */
		}
	}
}

/*====================================*/
void split(unsigned int inode, unsigned int child, int pos, unsigned int leaf_str)
/*====================================*/
{
	unsigned int child_sibling=get_sibling(child), new_first_child, 
		new_sibling, prev_child=0, aux_child=get_first_child(inode);

	if(in_str[leaf_str]<in_str[get_head(child)+get_inode_depth(inode)+pos]){
		/* Create a leaf with sibling child */
		create_master_leaf(child);
		/* Calculate the new first child of inode and new sibling of first child */
		new_first_child=LEAF_BIT|last_leaf_created;
		new_sibling=child;
	} else {
		/* Create a leaf without sibling (the new leaf is last/second child of inode) */
		create_master_leaf(0);
		/* Calculate the new first child of inode and new sibling of first child */
		new_first_child=child;
		new_sibling=LEAF_BIT|last_leaf_created;
	}

	/* Create the new node */
	create_small_inode(new_first_child,child_sibling);
	if(small_inodes_created==32)
		enlarge_inode(get_inode_depth(inode)+pos,last_leaf_created);
	else {
		depth_in_phase[last_inode_created-last_large_inode_created-1]=get_inode_depth(inode)+pos;
		head_in_phase[last_inode_created-last_large_inode_created-1]=last_leaf_created;
	}

	/* Find the previous child of child (to update sibling to new node) */
	while(aux_child!=child){
		prev_child=aux_child;
		aux_child=get_sibling(aux_child);
	}
	if(prev_child){
		if(is_leaf(prev_child)){
			int prev_child_index= prev_child & (~LEAF_BIT);
			if(! (leaves[prev_child_index] & MASTER_LEAF_BIT) ) 
				prev_child_index=leaves_master[prev_child_index];
			leaves[prev_child_index]&=0xE0000000; /* Delete sibling */
			leaves[prev_child_index]|=last_inode_created; /* Set sibling */
		} else {
			inodes[prev_child][1]&=0xE0000000; /* Delete sibling */
			inodes[prev_child][1]|=last_inode_created; /* Set sibling */
		}
	} else {
		/* Delete first child */
		inodes[inode][0]&=0xF8000000; 
		inodes[inode][1]&=0x3FFFFFFF;
		/* Set first child */
		inodes[inode][0]|=(last_inode_created >> 2);
		inodes[inode][1]|=(last_inode_created << 30);
	}

	if(is_leaf(child)){

		int child_index= child & (~LEAF_BIT);
		if(! (leaves[child_index] & MASTER_LEAF_BIT) ) 
			child_index=leaves_master[child_index];

		/* Possible suffix link info to pass from child to new node */
		if(!child_sibling){
			unsigned int suffix_link;
			suffix_link=((leaves[child_index] << 3) >> 3);
			leaves[child_index]&=0xE0000000;
			inodes[last_inode_created][1]|=suffix_link;
		}

		/* Update sibling of child */
		if(child==new_first_child) {
			leaves[child_index]&=0xE0000000;
			leaves[child_index]|=SIBLING_BIT;
			leaves[child_index]|=(LEAF_BIT|last_leaf_created);
		} else leaves[child_index]&=0xC0000000; /* Delete leaf bit sibling and sibling */

	} else {

		/* Possible suffix link info to pass from child to new inode */
		if(!child_sibling){
			unsigned int suffix_link;
			suffix_link=((inodes[child][1] << 3) >> 3);
			inodes[child][1]&=0xE0000000;
			inodes[last_inode_created][1]|=suffix_link;
		}

		/* Update sibling of child */
		if(child==new_first_child) {
			inodes[child][1]&=0xE0000000;
			inodes[child][1]|=SIBLING_BIT;
			inodes[child][1]|=(LEAF_BIT|last_leaf_created);
		} else inodes[child][1]&=0xC0000000; /* Delete leaf bit sibling and sibling */

	}
}

/*====================================*/
void add_string(unsigned int inode, int start, int end)
/*====================================*/
{
	int end_jump=0, length;
	unsigned int child=0;

	while(!end_jump && (end-start)){
		child=get_child(inode,in_str[start-1]);
		length=get_depth_in_construction(child)-get_inode_depth(inode);
		if((end-start) >= length){
			start+=length;
			inode=child;
		} else end_jump=1;
	}

	/* Ukkonnen's control variables */
	last_inode_reached=inode;
	undef_sl_father_inode=0;

	if (!(end-start)){
		child=get_child(inode,in_str[end-1]);
		if(!child) {
			add_child(inode,in_str[end-1]);
			/* Ukkonnen's control variables */
			if (inode) branching=1;
			father_inode=inode;
			/* Factor tree control variables */
			was_leaf_created=1;
			last_in_queue++;
		} else {
			/* Factor tree control variables */
			was_leaf_created=0;
			last_position_reached=end-start+1;
			last_leaf_reached=child;
			father_last_leaf_reached=inode;
		}
	} else {
		int begin_edge_pos=get_head(child)+get_inode_depth(inode);
		if(in_str[begin_edge_pos+(end-start)] != in_str[end-1]) {
			split(inode,child,end-start,end-1);
			/* Ukkonnen's control variables */
			new_inode_created=last_inode_created;
			branching=1;
			last_inode_reached=last_inode_created;
			father_inode=last_inode_created;
			grand_father_inode=inode;
			undef_sl_father_inode=1;
			/* Factor tree control variables */
			was_leaf_created=1;
			last_in_queue++;
		} else {
			/* Factor tree control variables */
			was_leaf_created=0;
			last_position_reached=end-start+1;
			last_leaf_reached=child;
			father_last_leaf_reached=inode;
		}
	}
}

/*====================================*/
unsigned int get_suffix_link_on_rightmost_child(unsigned int first_child)
/*====================================*/
{
	if(is_leaf(first_child)){
		int first_child_index=first_child&(~LEAF_BIT);
		if(! (leaves[first_child_index] & MASTER_LEAF_BIT) ) 
			first_child_index=leaves_master[first_child_index];
		if(has_sibling(leaves[first_child_index]))
			return get_suffix_link_on_rightmost_child((leaves[first_child_index] << 3) >> 3);
			/* Eventual optimization with &0x1FFFFFFF */
		else
			return ((leaves[first_child_index] << 3) >> 3);
	} else {
		if(has_sibling(inodes[first_child][1]))
			return get_suffix_link_on_rightmost_child((inodes[first_child][1] << 3) >> 3);
			/* Eventual optimization with &0x1FFFFFFF */
		else
			return ((inodes[first_child][1] << 3) >> 3);
	}
}

/*====================================*/
unsigned int get_suffix_link(unsigned int inode)
/*====================================*/
{
	if(is_small_inode(inode)) return inode+1;
	else {

		unsigned int depth=get_inode_depth(inode);
		unsigned int *cur_inode=inodes[inode];

		/* If inode <= 2^26 we use C1 + C2 scheme without using 2 bits of the leaf */
		if(inode <= 0x4000000){
			/* If depth < 2^10 we use C1 + D */
			if(depth < 0x400) {
				/* The suffix_link is coded on the 2..22 bits of C1 and first 5 bits of D */
				return (((cur_inode[2] << 1) >> 11) << 5) | (cur_inode[3] >> 27); /* Eventual optimization */
			} else {
				/* The suffix_link is coded on rightmost child (B scheme) whose field 
					 right sibling is nil */
				return get_suffix_link_on_rightmost_child(get_first_child(inode));
			}
		} else { 
			/* Else the last_inode_created > 2^26 we use C1' + C2 scheme without the 2 bits of the leaf */
			/* If depth < 2^8 we use C1' + D */
			if(depth < 0x100) {
				/* The suffix_link is coded on the 2..24 bits of C1' and first 5 bits of D */
				return (((cur_inode[2] << 1) >> 9) << 5) | (cur_inode[3] >> 27); /* Eventual optimization */	
			} else {
				/* The suffix_link is coded on rightmost child (B scheme) whose 
					 field right sibling is nil */
				return get_suffix_link_on_rightmost_child(get_first_child(inode));
			}
		}
	}
}

/*====================================*/
void initiate_suffix_tree()
/*====================================*/
{
	if(cur_str==1){ /* First string of the set of input string */

		/* Ukkonen's control variables */
		phase=2;
		father_inode=0;
		grand_father_inode=0;
		undef_sl_father_inode=0;
		prev_step_inode=0;
		last_inode_reached=0;
		new_inode_created=0;
		/* Factor tree control variables */
		first_in_queue=0;
		last_in_queue=0;

		create_master_leaf(0);
		create_small_inode(0|LEAF_BIT,0);
		enlarge_inode(0,0);

	} else { /* Strings 2..N of the input set */

		int end_jump=0, length, str_pos=offset[cur_str-1], i;
		unsigned int inode=0, child, begin_edge_pos, inode_depth, pos=0;

		while(!end_jump){

			child=get_child(inode,in_str[str_pos+pos]);

			if(child) {
			
				inode_depth=get_inode_depth(inode);
				begin_edge_pos=get_head(child)+inode_depth;
				length=get_depth_in_construction(child)-inode_depth;

				i=0;
				while(pos<k-1 && i<length){
					if (in_str[begin_edge_pos+i]!=in_str[str_pos+pos]) break;
					i++;
					pos++;
				}

				if (pos==k-1) {

					/* Ukkonen's control variables */
					phase=str_pos+k;
					father_inode=inode;
					grand_father_inode=0;
					undef_sl_father_inode=0;
					prev_step_inode=0;
					last_inode_reached=inode;
					new_inode_created=0;
					/* Factor tree control variables: first_in_queue and last_in_queue 
						 remain as before (the queue is empty) */
					was_leaf_created=0;
					last_position_reached=i;
					last_leaf_reached=child;
					father_last_leaf_reached=inode;

					end_jump=1;

					/* Go to build_factor_tree() */

				} else if(i>=length) {

					if (is_leaf(child)) {/* Case when k > length(cur_str) */
						end_jump=1; 
						phase=str_pos+k;
					} else 
						inode=child;

					/* Continue in the cycle */
					
				} else { /* if (in_str[begin_edge_pos+i] != in_str[str_pos+pos]) */

					/* Need to have a last leaf created before entering the cycle of the
						 suffix tree algorithm: split(inode,child,i,str_pos+pos) */
					phase=str_pos+pos+1;
					add_string(inode,phase-i,phase);
					if(new_inode_created) {
						prev_step_inode=new_inode_created;
						new_inode_created=0;
					}

					end_jump=1;

					/* Go to build_suffix_tree() */

				}
			
			} else {

				/* Need to have a last leaf created before entering the cycle of the
					 suffix tree algorithm: add_child(inode,str_pos+pos) */
				phase=str_pos+pos+1;
				add_string(inode,phase,phase);
				/* No new_inode_created nor branching so pass to next phase */
				if (!inode) phase++;

				end_jump=1;
			
				/* Go to build_suffix_tree() */

			}
		}
	}
}

/*====================================*/
void build_suffix_tree()
/*====================================*/
{
	int end_phase,forward;

	/* For cur_str=1, initializes the generalized factor tree
	   For cur_str>1, compute j such that the prefix s_{cur_str}[1..j] is already 
		 in the generalized factor tree (1<=j<k) */
	initiate_suffix_tree();

	for(;phase<offset[cur_str-1]+k && phase<=offset[cur_str];phase++){
		end_phase=0;
		do{
			branching=0;
			forward=((phase-last_leaf_created)-get_inode_depth(father_inode))-1;
			if(undef_sl_father_inode && father_inode){
				forward+=get_inode_depth(father_inode)-get_inode_depth(grand_father_inode);
				if(!grand_father_inode) 
					add_string(0,phase-forward+1,phase);
				else
					add_string(get_suffix_link(grand_father_inode),phase-forward,phase);
			} else {
				if (!father_inode) 
					add_string(0,phase-forward+1,phase);
				else
					add_string(get_suffix_link(father_inode),phase-forward,phase);
			}
			if(prev_step_inode) {
				/* Enlarge the last node of the phase */
				if((!branching || prev_step_inode+1!=last_inode_reached) 
					&& is_small_inode(last_inode_created))
					enlarge_inode(
						depth_in_phase[last_inode_created-last_large_inode_created-1],
						head_in_phase[last_inode_created-last_large_inode_created-1]);
				if(!is_small_inode(prev_step_inode))
					set_suffix_link(prev_step_inode,last_inode_reached);
				prev_step_inode=0;
			}
			if(!branching) end_phase=1;
			else if(new_inode_created) {
				prev_step_inode=new_inode_created;
				new_inode_created=0;
			}
		} while(!end_phase); 
	}
}


/****************************************************************/
/****************************************************************/
/*				FACTOR TREE (Allali's algorithm)				*/
/****************************************************************/
/****************************************************************/


/*====================================*/
void replace_child_leaf(unsigned int father, unsigned int old_child_leaf)
/*====================================*/
{
	unsigned int child=get_first_child(father), prev_child=0, new_child=LEAF_BIT|last_leaf_created;

	while(child!=old_child_leaf){
		prev_child=child;
		child=get_sibling(child);
	}

	if(!prev_child){ /* New first child */
		/* Delete first child */
		inodes[father][0]&=0xF8000000;
		inodes[father][1]&=0x3FFFFFFF;
		/* Set first child */
		inodes[father][0]|=(new_child >> 2);
		inodes[father][1]|=(new_child << 30);
	} else { /* New sibling */
		if(is_leaf(prev_child)){
			int prev_child_index= prev_child & (~LEAF_BIT);
			if(! (leaves[prev_child_index] & MASTER_LEAF_BIT) ) 
				prev_child_index=leaves_master[prev_child_index]; 
			leaves[prev_child_index]&=0xE0000000; /* Delete sibling */
			leaves[prev_child_index]|=new_child; /* Set sibling */
		} else {
			inodes[prev_child][1]&=0xE0000000; /* Delete sibling */
			inodes[prev_child][1]|=new_child; /* Set sibling */
		}
	}
}

/*====================================*/
void init_gen_factor_tree()
/*====================================*/
{
	leaves=(unsigned int *)malloc(num_leaves*sizeof(unsigned int));
	memset((void *) leaves,0,num_leaves*sizeof(unsigned int));
	leaves_master=(unsigned int *)malloc(num_leaves*sizeof(unsigned int));
	memset((void *) leaves_master,0,num_leaves*sizeof(unsigned int));
	inodes=(unsigned int **)malloc(num_leaves*sizeof(unsigned int *));
	memset((void *) inodes,0,num_leaves*sizeof(unsigned int *));
	last_inode_created=-1;
	last_leaf_created=-1;
	master_leaves_created=-1;
	small_inodes_created=0;
#ifdef DEBUG_STATISTICS
	large_inodes_created=0;
#endif
}

/*====================================*/
void clean_gen_factor_tree()
/*====================================*/
{
/* TODO - depending on su release or already released
	int i;
	free(leaves);
	free(leaves_master);
	for(i=0;i<last_inode_created+1;i++) free(inodes[i]);
	free(inodes);
*/
}

/*====================================*/
void finalize_factor_tree()
/*====================================*/
{
	/* Need slave leaves from last_leaf_created+1..offset[cur_str]-1 */
	if (last_leaf_created<(int)offset[cur_str]-1) {
	
		int pos=0;
		unsigned int new_leaf_index=last_leaf_created+1, inode=0, child;

		/* Add the first slave leaf missing */
		child=get_child(inode,in_str[new_leaf_index]);
		while(!is_leaf(child)){
			pos+=get_depth_in_construction(child)-get_inode_depth(inode);
			inode=child;
			child=get_child(inode,in_str[new_leaf_index+pos]);
		}
		create_slave_leaf(child);
		replace_child_leaf(inode,child);

		/* Follow suffix-links to add remaing leaves */
		while(get_leaf_depth_in_construction(last_leaf_created)>1){

			new_leaf_index=last_leaf_created+1;

			if(inode) { /* Follow suffix link and update variables */
				inode=get_suffix_link(inode);
				pos=get_inode_depth(inode);
			} else 
				pos=0;

			child=get_child(inode,in_str[new_leaf_index+pos]);
			while(!is_leaf(child)){
				pos+=get_depth_in_construction(child)-get_inode_depth(inode);
				inode=child;
				child=get_child(inode,in_str[new_leaf_index+pos]);
			}
			create_slave_leaf(child);
			replace_child_leaf(inode,child);
		}

	}

	/* Empty queue */
	first_in_queue=last_in_queue+1;
}

/*====================================*/
void build_factor_tree()
/*====================================*/
{
	int end_phase,forward;

	/* Factor tree control variables */
	was_leaf_created=1;

	for(phase=offset[cur_str-1]+k;phase<=offset[cur_str];phase++){
		end_phase=0;
		/* Factor tree */
		if (first_in_queue>last_in_queue) { /* Queue is empty */
			add_string(last_inode_reached,phase-last_position_reached,phase);
			if (!was_leaf_created) {
				create_slave_leaf(last_leaf_reached);
				father_inode= father_last_leaf_reached;
				replace_child_leaf(father_inode,last_leaf_reached);
			}
			if(new_inode_created) {
				prev_step_inode=new_inode_created;
				new_inode_created=0;
			}
		}
		do{
			/* Suffix tree */
			branching=0;
			forward=(get_leaf_depth_in_construction(last_leaf_created)-get_inode_depth(father_inode))-1;
			if(undef_sl_father_inode && father_inode){
				forward+=get_inode_depth(father_inode)-get_inode_depth(grand_father_inode);
				if(!grand_father_inode) 
					add_string(0,phase-forward+1,phase);
				else
					add_string(get_suffix_link(grand_father_inode),phase-forward,phase);
			} else {
				if (!father_inode) 
					add_string(0,phase-forward+1,phase);
				else
					add_string(get_suffix_link(father_inode),phase-forward,phase);
			}
			if(prev_step_inode) {
				/* Enlarge the last node of the phase */
				if((!branching || prev_step_inode+1!=last_inode_reached) 
					&& is_small_inode(last_inode_created))
					enlarge_inode(
						depth_in_phase[last_inode_created-last_large_inode_created-1],
						head_in_phase[last_inode_created-last_large_inode_created-1]);
				if(!is_small_inode(prev_step_inode))
					set_suffix_link(prev_step_inode,last_inode_reached);
				prev_step_inode=0;
			}
			if(!branching) end_phase=1;
			else if(new_inode_created) {
				prev_step_inode=new_inode_created;
				new_inode_created=0;
			}
		} while(!end_phase); 
		/* Factor tree: remove the leaf at the head of the queue */
		if (first_in_queue<=last_in_queue) first_in_queue++;
	}

	/* Add possible slave leaves to the generalized factor tree */
	finalize_factor_tree();
}

/*====================================*/
void build_gen_factor_tree()
/*====================================*/
{	
	if(!num_leaves){
		printf("Error: Building tree without parsing");
		exit(1);
	}

	init_gen_factor_tree();

	/* Strings 1..N in the input set */
	for(cur_str=1;cur_str<=N;cur_str++) {
		/* Build generalized factor tree for s_{cur_str}[1..k-1] */ 
		build_suffix_tree();
		/* Build generalized factor tree for s_{cur_str}[k..n] */ 
		build_factor_tree();
	}

	inodes=(unsigned int **)realloc((void *)inodes,(last_inode_created+1)*sizeof(unsigned int *));

#ifdef DEBUG_STATISTICS
	printf("\t nb large nodes:  \t%d\n",large_inodes_created);
	printf("\t nb small nodes:  \t%d\n",last_inode_created-large_inodes_created);
	printf("\t nb slave leaves: \t%d\n",last_leaf_created-master_leaves_created);
	printf("\t nb master leaves:\t%d\n",master_leaves_created);
#endif
}


/****************************************************************/
/****************************************************************/
/*							COLORS								*/
/****************************************************************/
/****************************************************************/


/*====================================*/
void init_colors()
/*====================================*/
{
	leaves_color=(Bit_Tab***)malloc(sizeof(Bit_Tab**)*params.p);
	memset((void *)leaves_color,0,sizeof(Bit_Tab**)*params.p);
	leaves_color[0]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(master_leaves_created+1));
	memset((void *)leaves_color[0],0,sizeof(Bit_Tab*)*(master_leaves_created+1));
	
	inodes_color=(Bit_Tab***)malloc(sizeof(Bit_Tab**)*params.p);
	memset((void *)inodes_color,0,sizeof(Bit_Tab**)*params.p);
    inodes_color[0]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(last_inode_created+1));
	memset((void *)inodes_color[0],0,sizeof(Bit_Tab*)*(last_inode_created+1));
	
	masters_depth=(unsigned int*)malloc(sizeof(unsigned int)*(master_leaves_created+1));
	memset((void *)masters_depth,0,sizeof(unsigned int)*(master_leaves_created+1));

	nb_leaves_color=(unsigned short**)malloc(sizeof(unsigned short*)*params.p);
	memset((void *)nb_leaves_color,0,sizeof(unsigned short*)*params.p);
	nb_leaves_color[0]=(unsigned short*)malloc(sizeof(unsigned short)*(master_leaves_created+1));
	memset((void *)nb_leaves_color[0],0,sizeof(unsigned short)*(master_leaves_created+1));

	nb_inodes_color=(unsigned short**)malloc(sizeof(unsigned short*)*params.p);
	memset((void *)nb_inodes_color,0,sizeof(unsigned short*)*params.p);
	nb_inodes_color[0]=(unsigned short*)malloc(sizeof(unsigned short)*(last_inode_created+1));
	memset((void *)nb_inodes_color[0],0,sizeof(unsigned short)*(last_inode_created+1));
}

/*===========================================================*/
void build_leaves_colors()
/*===========================================================*/
{
	unsigned int leaf_index, master_leaf_index=0, i; int j;

	for (i=0;i<N;i++) {
		for (leaf_index=offset[i];leaf_index<offset[i+1];leaf_index++) {
			if (leaves[leaf_index] & MASTER_LEAF_BIT) {
				/* Create color for the master leaf */
				leaves_color[0][master_leaf_index]=AllocBitTab();
				addBitTabValue(&leaves_color[0][master_leaf_index],i);
				/* Update leaves' master and set masters' depth */
				masters_depth[master_leaf_index]=leaves_master[leaf_index];
				leaves_master[leaf_index]=master_leaf_index;
				master_leaf_index++;
			} else
				addBitTabValue(&leaves_color[0][leaves_master[leaves_master[leaf_index]]],i);
		}
	}

	for (j=0;j<master_leaves_created+1;j++)
		nb_leaves_color[0][j]=nbSequenceInBitTab(leaves_color[0][j]);
}

/*===========================================================*/
void build_node_colors(unsigned int node, Bit_Tab **color)
/*===========================================================*/
{
    if (is_leaf(node)) {

		unsigned int leaf_index=(node &~LEAF_BIT);

		if (leaves[leaf_index] & MASTER_LEAF_BIT)
			(*color)=leaves_color[0][leaves_master[leaf_index]];
		else
			(*color)=leaves_color[0][leaves_master[leaves_master[leaf_index]]];

    } else {

        unsigned int child;
        Bit_Tab *child_color;

        (*color)=AllocBitTab();
        for (child=get_first_child(node);child;child=get_sibling(child)) {
            build_node_colors(child,&child_color);
            fusionneBitTab(color,child_color);
        }
        inodes_color[0][node]=(*color);
		nb_inodes_color[0][node]=nbSequenceInBitTab(inodes_color[0][node]);

	}
}

/*====================================*/
void build_colors()
/*====================================*/
{
	Bit_Tab* color;

	initBitTab(N);
	init_colors();
	build_leaves_colors();
	build_node_colors(0,&color);

#ifdef DEBUG_STATISTICS
	printf("\t number of colors in the root is %d (in %d sequences)\n",nbSequenceInBitTab(inodes_color[0][0]),N);
#endif
}

/*====================================*/
void clean_colors()
/*====================================*/
{
	int i,j;
	for (i=0;i<=master_leaves_created;i++) free(leaves_color[0][i]);
	free(leaves_color[0]);
	for (j=1;j<params.p;j++) {
		if (leaves_color[j]) free(leaves_color[j]);
		else break;
	}
	free(leaves_color);
	for (i=0;i<=last_inode_created;i++) free(inodes_color[0][i]);
	free(inodes_color[0]);
	for (j=1;j<params.p;j++) {
		if (inodes_color[j]) free(inodes_color[j]);
		else break;
	}
	free(inodes_color);
	free(masters_depth);
}


/****************************************************************/
/****************************************************************/
/*						SPEED UP TEST							*/
/****************************************************************/
/****************************************************************/


/*====================================*/
unsigned int su_get_first_child(unsigned int inode)
/*====================================*/
{
	return su_inode_first_child[inode];
}

/*====================================*/
unsigned int su_get_sibling(unsigned int node)
/*====================================*/
{
	if (is_leaf(node)) 
		return su_leaf_sibling[su_leaf_master_index[node&~LEAF_BIT]];
	else 
		return su_inode_sibling[node];
}

/*====================================*/
unsigned int su_get_inode_depth(unsigned int inode)
/*====================================*/
{
	return su_inode_depth[inode];
}

/*====================================*/
unsigned int su_get_depth(unsigned int node)
/*====================================*/
{
	if (is_leaf(node)) 
		return su_leaf_depth[su_leaf_master_index[node&~LEAF_BIT]];
	else return 
		su_inode_depth[node];
}

/*====================================*/
unsigned int su_get_head(unsigned int node)
/*====================================*/
{
	if (is_leaf(node)) 
		return (node&~LEAF_BIT);
	else 
		return su_inode_head[node];
}

#ifdef PURGE_OPTIMIZATION
/*====================================*/
unsigned int purge_leaf(unsigned int leaf_index)
/*====================================*/
{
	unsigned int prev=leaf_index, cur=su_leaf_next_leaf[leaf_index], next;

	while (prev!=cur) {
		next=su_leaf_next_leaf[cur];
		if (cur+g_disposable_distance>=offset[su_leaf_in_str[cur]+1]) { /* Leaf cur is to be disposed */
#ifdef DEBUG_STATISTICS
			g_nb_dispoded_leaves++;
#endif
			if (cur==next) { /* Leaf cur is the master - end of list */
				su_leaf_next_leaf[prev]=prev; /* Marking end of list in prev */
				break;
			}
			su_leaf_next_leaf[prev]=next; /* Dispose cur */
			cur=next;
		} else { /* Leaf cur is to be considered */
			prev=cur;
			cur=next;
		}
	}

	/* The returned value is the leaf that should replace the leaf being considered as
		 first-child/right-sibling of parent/left-sibling node in the factor tree, 
		 since the leaf being pointed is a disposable one (cannot extract a model)
	   If no replace is needed 0 is returned */
	if (leaf_index+g_disposable_distance>=offset[su_leaf_in_str[leaf_index]+1] &&
		su_leaf_next_leaf[leaf_index]!=leaf_index) 
		return (su_leaf_next_leaf[leaf_index]|LEAF_BIT);
	else
		return 0;

} /* #ifdef PURGE_OPTIMIZATION */
/*====================================*/
unsigned int purge_next_leaves(unsigned int node)
/*====================================*/
{
	if (is_leaf(node)) 
		return purge_leaf(node&~LEAF_BIT);
	else {
        unsigned int child, prev_child=0; unsigned int res;
        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
			if ((res=purge_next_leaves(child))) {
				if (prev_child) { /* Not the first child */
					if (is_leaf(prev_child))
						su_leaf_sibling[su_leaf_master_index[prev_child&~LEAF_BIT]]=res;
					else
						su_inode_sibling[prev_child]=res;
				} else /* The first child */
					su_inode_first_child[node]=res;
			} 
			prev_child=child;
		}
	}
	return 0;
}
#endif /* #ifdef PURGE_OPTIMIZATION */

/*====================================*/
void init_su_tables()
/*====================================*/
{
	int i; unsigned int j,m,master_leaf_index;

	su_inode_first_child=(unsigned int*)malloc(sizeof(unsigned int)*(last_inode_created+1));
	memset((void *)su_inode_first_child,0,sizeof(unsigned int)*(last_inode_created+1));
	su_inode_sibling=(unsigned int*)malloc(sizeof(unsigned int)*(last_inode_created+1));
	memset((void *)su_inode_sibling,0,sizeof(unsigned int)*(last_inode_created+1));
	su_inode_head=(unsigned int*)malloc(sizeof(unsigned int)*(last_inode_created+1));
	memset((void *)su_inode_head,0,sizeof(unsigned int)*(last_inode_created+1));
	su_inode_depth=(unsigned short*)malloc(sizeof(unsigned short)*(last_inode_created+1));
	memset((void *)su_inode_depth,0,sizeof(unsigned short)*(last_inode_created+1));

	for (i=0;i<last_inode_created+1;i++) {
		su_inode_first_child[i]=get_first_child(i);
		su_inode_sibling[i]=get_sibling(i);
		su_inode_head[i]=get_inode_head(i);
		su_inode_depth[i]=(unsigned short)get_inode_depth(i);
	}

	for (i=0;i<last_inode_created+1;i++) free(inodes[i]);
	free(inodes);

	su_leaf_sibling=(unsigned int*)malloc(sizeof(unsigned int)*(master_leaves_created+1));
	memset((void *)su_leaf_sibling,0,sizeof(unsigned int)*(master_leaves_created+1));
	su_leaf_master_index=(unsigned int*)malloc(sizeof(unsigned int)*(last_leaf_created+1));
	memset((void *)su_leaf_master_index,0,sizeof(unsigned int)*(last_leaf_created+1));
	su_leaf_depth=(unsigned short*)malloc(sizeof(unsigned short)*(master_leaves_created+1));
	memset((void *)su_leaf_depth,0,sizeof(unsigned short)*(master_leaves_created+1));
	su_leaf_next_leaf=(unsigned int*)malloc(sizeof(unsigned int)*(last_leaf_created+1));
	memset((void *)su_leaf_next_leaf,0,sizeof(unsigned int)*(last_leaf_created+1));
	su_leaf_in_str=(unsigned int*)malloc(sizeof(unsigned int)*(last_leaf_created+1));
	memset((void *)su_leaf_in_str,0,sizeof(unsigned int)*(last_leaf_created+1));

	master_leaf_index=0;
	for (j=0;j<N;j++) {
		for (m=offset[j];m<offset[j+1];m++) {
			if(leaves[m] & MASTER_LEAF_BIT) {
				su_leaf_master_index[m]=master_leaf_index;
				su_leaf_depth[master_leaf_index]=leaves_master[m];
				master_leaf_index++;
				su_leaf_next_leaf[m]=m; /* Represents the end of the list of next leaves */
				su_leaf_in_str[m]=j;
			} else {
				su_leaf_master_index[m]=su_leaf_master_index[leaves_master[m]];
				su_leaf_next_leaf[m]=(leaves[m]&~LEAF_BIT);
				su_leaf_in_str[m]=j;
			}
			if (!su_leaf_sibling[su_leaf_master_index[m]])
				su_leaf_sibling[su_leaf_master_index[m]]=get_sibling(m|LEAF_BIT);
		}
	}

	free(leaves);
	free(leaves_master);

	g_disposable_distance=0;
#ifdef PURGE_OPTIMIZATION
	{
		unsigned short l;
		for (l=0;l<params.p-1;l++)
			g_disposable_distance+=params.k_min[l]+params.d_min[l];
		g_disposable_distance+=params.k_min[params.p-1];
		purge_next_leaves(0);
	}
#ifdef DEBUG_STATISTICS
	printf("\t number of leaves disposed in purge is %d (in %d leaves)\n",g_nb_dispoded_leaves,last_leaf_created+1);
#endif /* #ifdef DEBUG_STATISTICS */
#endif /* #ifdef PURGE_OPTIMIZATION */
}

/*====================================*/
void clean_su_tables()
/*====================================*/
{
	free(su_inode_first_child);
	free(su_inode_sibling);
	free(su_inode_head);
	free(su_inode_depth);
	free(su_leaf_sibling);
	free(su_leaf_master_index);
	free(su_leaf_depth);
#ifndef STATIC_BOX_LINKS
	free(su_leaf_next_leaf);
	free(su_leaf_in_str);
#endif /* #ifndef STATIC_BOX_LINKS */
}

/*====================================*/
void su_build_node_colors(unsigned int node, Bit_Tab **color)
/*====================================*/
{
    if (is_leaf(node)) {

		unsigned int leaf_index=(node &~LEAF_BIT), 
			master=su_leaf_master_index[leaf_index],
			cur=leaf_index, next=su_leaf_next_leaf[leaf_index];

		if (leaf_index+g_disposable_distance>=offset[su_leaf_in_str[leaf_index]+1]) {
			(*color)=leaves_color[0][master]; /* NULL color */
			return;
		}

		leaves_color[0][master]=AllocBitTab();
		addBitTabValue(&(leaves_color[0][master]),su_leaf_in_str[cur]);
		while (cur!=next) {
			addBitTabValue(&(leaves_color[0][master]),su_leaf_in_str[next]);
			cur=next;
			next=su_leaf_next_leaf[next];
		}

		(*color)=leaves_color[0][master];
		nb_leaves_color[0][master]=nbSequenceInBitTab(leaves_color[0][master]);

    } else {

        unsigned int child;
        Bit_Tab *child_color;
		(*color)=NULL;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            su_build_node_colors(child,&child_color);
            if (child_color) {
				if (!(*color)) {
					(*color)=AllocBitTab();
					CopyBitTab(color,child_color);
				} else fusionneBitTab(color,child_color);
			}
        }
		if (*color) {
			inodes_color[0][node]=(*color);
			nb_inodes_color[0][node]=nbSequenceInBitTab(inodes_color[0][node]);
		}

	}
}

/*====================================*/
void su_init_colors()
/*====================================*/
{
	leaves_color=(Bit_Tab***)malloc(sizeof(Bit_Tab**)*params.p);
	memset((void *)leaves_color,0,sizeof(Bit_Tab**)*params.p);
	leaves_color[0]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(master_leaves_created+1));
	memset((void *)leaves_color[0],0,sizeof(Bit_Tab*)*(master_leaves_created+1));
	
	inodes_color=(Bit_Tab***)malloc(sizeof(Bit_Tab**)*params.p);
	memset((void *)inodes_color,0,sizeof(Bit_Tab**)*params.p);
    inodes_color[0]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(last_inode_created+1));
	memset((void *)inodes_color[0],0,sizeof(Bit_Tab*)*(last_inode_created+1));
	
	nb_leaves_color=(unsigned short**)malloc(sizeof(unsigned short*)*params.p);
	memset((void *)nb_leaves_color,0,sizeof(unsigned short*)*params.p);
	nb_leaves_color[0]=(unsigned short*)malloc(sizeof(unsigned short)*(master_leaves_created+1));
	memset((void *)nb_leaves_color[0],0,sizeof(unsigned short)*(master_leaves_created+1));

	nb_inodes_color=(unsigned short**)malloc(sizeof(unsigned short*)*params.p);
	memset((void *)nb_inodes_color,0,sizeof(unsigned short*)*params.p);
	nb_inodes_color[0]=(unsigned short*)malloc(sizeof(unsigned short)*(last_inode_created+1));
	memset((void *)nb_inodes_color[0],0,sizeof(unsigned short)*(last_inode_created+1));

	if (params.p>2) {
		reached_leaves=(leaf_list**)malloc(sizeof(leaf_list*)*params.p-2);
		memset((void *)reached_leaves,0,sizeof(leaf_list*)*params.p-2);
	}
}

/*====================================*/
void su_build_colors()
/*====================================*/
{
	Bit_Tab* color;

	initBitTab(N);
	su_init_colors();
	su_build_node_colors(0,&color);

#ifdef STATIC_BOX_LINKS
	free(su_leaf_next_leaf);
	free(su_leaf_in_str);
#endif /* #ifdef STATIC_BOX_LINKS */

#ifdef DEBUG_STATISTICS
	printf("\t number of colors in the root is %d (in %d sequences)\n",nbSequenceInBitTab(inodes_color[0][0]),N);
#endif
}

/*===========================================================*/
void free_first_box_tree(unsigned int node)
/*===========================================================*/
{
    if (is_leaf(node)) {
		unsigned int leaf_index=su_leaf_master_index[(node &~LEAF_BIT)];
		if (leaves_color[0][leaf_index]) {
			free(leaves_color[0][leaf_index]);
			leaves_color[0][leaf_index]=NULL;
			nb_leaves_color[0][leaf_index]=0;
		}
	} else {
		if (nb_inodes_color[0][node]) {
			unsigned int child;
			for (child=su_get_first_child(node);child;child=su_get_sibling(child))
				free_first_box_tree(child);
			free(inodes_color[0][node]); 
			inodes_color[0][node]=NULL;
			nb_inodes_color[0][node]=0;
		}
    }
}

/*====================================*/
void su_clean_colors()
/*====================================*/
{
	int j;

	free_first_box_tree(0); /* Free leaves_colors[0][-] and inodes_colors[0][-] */

	free(leaves_color[0]);
	free(inodes_color[0]);
	for (j=1;j<params.p;j++) {
		if (leaves_color[j]) {
			free(leaves_color[j]);
			free(inodes_color[j]);
			free(nb_leaves_color[j]);
			free(nb_inodes_color[j]);
		}
		else break;
	}
	for (j=0;j<params.p-2;j++) {
		if (reached_leaves[j])
			free(reached_leaves[j]);
		else break;
	}
	free(leaves_color);
	free(inodes_color);
	free(nb_leaves_color);
	free(nb_inodes_color);
	free(reached_leaves);
}

/*====================================*/
Bit_Tab *su_get_color(unsigned short box, unsigned int node)
/*====================================*/
{
	if(is_leaf(node)) 
		return leaves_color[box][su_leaf_master_index[node&~LEAF_BIT]];
	else 
		return inodes_color[box][node];
}

/*====================================*/
unsigned int su_get_nb_colors(unsigned short box, unsigned int node)
/*====================================*/
{
	if(is_leaf(node)) 
		return (unsigned int) nb_leaves_color[box][su_leaf_master_index[node&~LEAF_BIT]];
	else 
		return (unsigned int) nb_inodes_color[box][node];
}


/****************************************************************/
/****************************************************************/
/*						FACTOR TREE API							*/
/****************************************************************/
/****************************************************************/


/*====================================*/
void init_box_colors(unsigned short box)
/*====================================*/
{
	if (leaves_color[box]) return;
	leaves_color[box]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(master_leaves_created+1));
	memset((void *)leaves_color[box],0,sizeof(Bit_Tab*)*(master_leaves_created+1));
	inodes_color[box]=(Bit_Tab**)malloc(sizeof(Bit_Tab*)*(last_inode_created+1));
	memset((void *)inodes_color[box],0,sizeof(Bit_Tab*)*(last_inode_created+1));
	nb_leaves_color[box]=(unsigned short*)malloc(sizeof(unsigned short)*(master_leaves_created+1));
	memset((void *)nb_leaves_color[box],0,sizeof(unsigned short)*(master_leaves_created+1));
	nb_inodes_color[box]=(unsigned short*)malloc(sizeof(unsigned short)*(last_inode_created+1));
	memset((void *)nb_inodes_color[box],0,sizeof(unsigned short)*(last_inode_created+1));
	if (box+1<params.p) {
		reached_leaves[box-1]=(leaf_list*)malloc(sizeof(leaf_list)*(master_leaves_created+1));
		memset((void *)reached_leaves[box-1],0,sizeof(leaf_list)*(master_leaves_created+1));
	}
}


/****************************************************************/
/****************************************************************/
/*							BOX-LINKS							*/
/****************************************************************/
/****************************************************************/


#ifdef STATIC_BOX_LINKS
/*====================================*/
void init_box_links()
/*====================================*/
{
	unsigned int i;
	leaves_box_links=(box_link_list**)malloc(sizeof(box_link_list*)*delta_k);
	memset((void*)leaves_box_links,0,sizeof(box_link_list*)*delta_k);
	for (i=0;i<delta_k;i++) {
		leaves_box_links[i]=(box_link_list*)malloc(sizeof(box_link_list)*(master_leaves_created+1));
		memset((void*)leaves_box_links[i],0,sizeof(box_link_list)*(master_leaves_created+1));
	}
} /* #ifdef STATIC_BOX_LINKS */
/*===========================================================*/
void add_leaf_box_link(unsigned int cur_delta_k, unsigned int from_leaf_index, unsigned int to_leaf_index, unsigned int nb_in_str)
/*===========================================================*/
{
	struct box_link* new_box_link;

	if (leaves_box_links[cur_delta_k][from_leaf_index]) {

		box_link_elem *cur_box_link=leaves_box_links[cur_delta_k][from_leaf_index];
		
		if (cur_box_link->blink->leaf==to_leaf_index) 
			addBitTabValue(&(cur_box_link->blink->color),nb_in_str);
		else if (cur_box_link->blink->leaf>to_leaf_index) {
			new_box_link=(struct box_link*)malloc(sizeof(struct box_link));
			new_box_link->leaf=to_leaf_index;
			new_box_link->color=AllocBitTab();
			addBitTabValue(&(new_box_link->color),nb_in_str);
			add_box_link(&(leaves_box_links[cur_delta_k][from_leaf_index]),new_box_link);
		} else {
			for (;cur_box_link->prox && cur_box_link->prox->blink->leaf<to_leaf_index; 
				  cur_box_link=cur_box_link->prox);
			if (cur_box_link->prox && cur_box_link->prox->blink->leaf==to_leaf_index)
				addBitTabValue(&(cur_box_link->prox->blink->color),nb_in_str);
			else {
				box_link_elem *new_box_link_elem=(box_link_elem*)malloc(sizeof(struct box_link_elem_struct));
				new_box_link=(struct box_link*)malloc(sizeof(struct box_link));
				new_box_link->leaf=to_leaf_index;
				new_box_link->color=AllocBitTab();
				addBitTabValue(&(new_box_link->color),nb_in_str);
				new_box_link_elem->blink=new_box_link;
				new_box_link_elem->prox=cur_box_link->prox;
				cur_box_link->prox=new_box_link_elem;
			}
		}
	} else {
		new_box_link=(struct box_link*)malloc(sizeof(struct box_link));
		new_box_link->leaf=to_leaf_index;
		new_box_link->color=AllocBitTab();
		addBitTabValue(&(new_box_link->color),nb_in_str);
		add_box_link(&(leaves_box_links[cur_delta_k][from_leaf_index]),new_box_link);
	}
} /* #ifdef STATIC_BOX_LINKS */
/*===========================================================*/
void build_leaves_box_links()
/*===========================================================*/
{
	unsigned int leaf_index, i, j, m;

	for (i=0;i<N;i++) {
		for (leaf_index=offset[i];leaf_index<offset[i+1];leaf_index++) {			
			/* Create box-links to each delta_k */
			for (m=0;m<delta_k;m++) {
				/* Create box-link if distance allows */
				for (j=params.d_min[0];j<=params.d_max[0];j++) {
					if (leaf_index+j+params.k_min[0]+m+params.k_min[1]<offset[i+1])
						add_leaf_box_link(
							m, /* Delta k being considered */
							su_leaf_master_index[leaf_index], /* From master leaf index */
							su_leaf_master_index[leaf_index+j+params.k_min[0]+m], /* To master leaf index */
							i); /* Input string number */
					else break;
				}
			}
		}
	}
} /* #ifdef STATIC_BOX_LINKS */
/*====================================*/
void build_box_links()
/*====================================*/
{
	if (params.p>1) {
		init_box_links();
		build_leaves_box_links();
	}
} /* #ifdef STATIC_BOX_LINKS */
/*====================================*/
void clean_box_links()
/*====================================*/
{
	if (params.p>1) {
		int i; unsigned int j;
		for (j=0;j<delta_k;j++) {
			for (i=0;i<master_leaves_created;i++)
				free_box_link_list(leaves_box_links[j][i]);
			free(leaves_box_links[j]);
		}
		free(leaves_box_links);
	}
} /* #ifdef STATIC_BOX_LINKS */
/*===========================================================*/
void update_leaf_tree(unsigned short box, unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node)) {

		box_link_elem *cur_box_link;		

		cur_box_link=leaves_box_links[cur_delta_k][su_leaf_master_index[node&~LEAF_BIT]];

		while (cur_box_link) {

			if (!leaves_color[box][cur_box_link->blink->leaf]) {
				leaves_color[box][cur_box_link->blink->leaf]=AllocBitTab();
				CopyBitTab(&(leaves_color[box][cur_box_link->blink->leaf]),cur_box_link->blink->color);
			} else
				fusionneBitTab(&(leaves_color[box][cur_box_link->blink->leaf]),cur_box_link->blink->color);

			cur_box_link=cur_box_link->prox;
		}

	} else {

        unsigned int child;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_leaf_tree(box,cur_delta_k,child);
        }
	}
} /* #ifdef STATIC_BOX_LINKS */
#endif /* #ifdef STATIC_BOX_LINKS */

#ifndef STATIC_BOX_LINKS
/*===========================================================*/
void update_dyad_leaf_tree(unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node)) {

		unsigned int leaf_index=node&~LEAF_BIT, j, cur=leaf_index, next=su_leaf_next_leaf[cur],
			depth=cur+params.k_min[0]+cur_delta_k;

		/* Create box-link (all distances are relevant) */
		for (j=params.d_min[0];j<=params.d_max[0];j++) {
			if (depth+j<offset[su_leaf_in_str[cur]+1]) {
				if (!leaves_color[1][su_leaf_master_index[depth+j]/*to*/])
					leaves_color[1][su_leaf_master_index[depth+j]]=AllocBitTab();
				addBitTabValue(&(leaves_color[1][su_leaf_master_index[depth+j]]),su_leaf_in_str[cur]);
#ifdef SMILE_OUTPUT
				nb_leaves_occ[su_leaf_master_index[depth+j]]++;
#endif /* #ifdef SMILE_OUTPUT */
			}
		}

		depth-=cur;

		while (cur!=next) {

			/* Create box-link (all distances are relevant) */
			for (j=params.d_min[0];j<=params.d_max[0];j++) {
				if (next+depth+j<offset[su_leaf_in_str[next]+1]) {
					if (!leaves_color[1][su_leaf_master_index[next+depth+j]/*to*/])
						leaves_color[1][su_leaf_master_index[next+depth+j]]=AllocBitTab();
					addBitTabValue(&(leaves_color[1][su_leaf_master_index[next+depth+j]]),su_leaf_in_str[next]);
#ifdef SMILE_OUTPUT
					nb_leaves_occ[su_leaf_master_index[next+depth+j]]++;
#endif /* #ifdef SMILE_OUTPUT */
				}
			}

			cur=next;
			next=su_leaf_next_leaf[next];

		}

	} else {

        unsigned int child;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_dyad_leaf_tree(cur_delta_k,child);
        }

	}
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void update_first_box_leaf_tree(unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node)) {

		unsigned int leaf_index=node&~LEAF_BIT, j, cur=leaf_index, next=su_leaf_next_leaf[cur],
			depth=cur+params.k_min[0]+cur_delta_k;

		/* Create box-link (all distances are relevant) */
		for (j=params.d_min[0];j<=params.d_max[0];j++) {
			if (depth+j<offset[su_leaf_in_str[cur]+1]) {
				if (!leaves_color[1][su_leaf_master_index[depth+j]/*to*/])
					leaves_color[1][su_leaf_master_index[depth+j]]=AllocBitTab();
				addBitTabValue(&(leaves_color[1][su_leaf_master_index[depth+j]]),su_leaf_in_str[cur]);
				add_leaf(&(reached_leaves[0][su_leaf_master_index[depth+j]/*to*/]),depth+j);
			}
		}

		depth-=cur;

		while (cur!=next) {

			/* Create box-link (all distances are relevant) */
			for (j=params.d_min[0];j<=params.d_max[0];j++) {
				if (next+depth+j<offset[su_leaf_in_str[next]+1]) {
					if (!leaves_color[1][su_leaf_master_index[next+depth+j]/*to*/])
						leaves_color[1][su_leaf_master_index[next+depth+j]]=AllocBitTab();
					addBitTabValue(&(leaves_color[1][su_leaf_master_index[next+depth+j]]),su_leaf_in_str[next]);
					add_leaf(&(reached_leaves[0][su_leaf_master_index[next+depth+j]/*to*/]),next+depth+j);
				}
			}

			cur=next;
			next=su_leaf_next_leaf[next];

		}

	} else {

        unsigned int child;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_first_box_leaf_tree(cur_delta_k,child);
        }

	}
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void update_middle_box_leaf_tree(unsigned short box, unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node)) {

		unsigned int j, depth=params.k_min[box-1]+cur_delta_k;
		leaf_list lleaf=reached_leaves[box-2][su_leaf_master_index[node&~LEAF_BIT]];

		/* Create box-link (all distances are relevant) */
		while (lleaf) {
			/* Create box-link (all distances are relevant) */
			for (j=params.d_min[box-1];j<=params.d_max[box-1];j++) {
				if (lleaf->leaf+depth+j<offset[su_leaf_in_str[lleaf->leaf]+1]) {
					if (!leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]/*to*/])
						leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]]=AllocBitTab();
					addBitTabValue(&(leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]]),su_leaf_in_str[lleaf->leaf]);
					add_leaf(&(reached_leaves[box-1][su_leaf_master_index[lleaf->leaf+depth+j]/*to*/]),lleaf->leaf+depth+j);
				}
			}
			lleaf=lleaf->prox;
		}

	} else {

        unsigned int child;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_middle_box_leaf_tree(box,cur_delta_k,child);
        }

	}
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void update_last_box_leaf_tree(unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node)) {

		unsigned int j, box=params.p-1, depth=params.k_min[box-1]+cur_delta_k;
		leaf_list lleaf=reached_leaves[box-2][su_leaf_master_index[node&~LEAF_BIT]];

		/* Create box-link (all distances are relevant) */
		while (lleaf) {
			/* Create box-link (all distances are relevant) */
			for (j=params.d_min[box-1];j<=params.d_max[box-1];j++) {
				if (lleaf->leaf+depth+j<offset[su_leaf_in_str[lleaf->leaf]+1]) {
					if (!leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]/*to*/])
						leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]]=AllocBitTab();
					addBitTabValue(&(leaves_color[box][su_leaf_master_index[lleaf->leaf+depth+j]]),su_leaf_in_str[lleaf->leaf]);
#ifdef SMILE_OUTPUT
					nb_leaves_occ[su_leaf_master_index[lleaf->leaf+depth+j]]++;
#endif /* #ifdef SMILE_OUTPUT */
				}
			}
			lleaf=lleaf->prox;
		}

	} else {

        unsigned int child;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_last_box_leaf_tree(cur_delta_k,child);
        }

	}
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void update_leaf_tree(unsigned short box, unsigned int cur_delta_k, unsigned int node)
/*===========================================================*/
{
	if (box==1) { /* Second box */
		if (params.p==2) {
			/* Optimized for dyads */
			update_dyad_leaf_tree(cur_delta_k,node);
		} else 
			/* Optimized for first box of an extraction with p>2 */
			update_first_box_leaf_tree(cur_delta_k,node);
	} else {
		if (box+1<params.p) 
			/* Optimized for middle box of an extraction with p>2 */
			update_middle_box_leaf_tree(box,cur_delta_k,node);
		else {
			/* Optimized for last box of an extraction with p>2 */
			update_last_box_leaf_tree(cur_delta_k,node);
		}
	}
} /* #ifndef STATIC_BOX_LINKS */
#endif /* #ifndef STATIC_BOX_LINKS */

/*===========================================================*/
void update_inode_colors(unsigned short box, unsigned int node, Bit_Tab **color)
/*===========================================================*/
{
    if (is_leaf(node)) {

		unsigned int leaf_index=su_leaf_master_index[(node &~LEAF_BIT)];
		(*color)=leaves_color[box][leaf_index];
		if (*color) nb_leaves_color[box][leaf_index]=nbSequenceInBitTab(*color);

    } else {

        unsigned int child;
        Bit_Tab *child_color=NULL;
		(*color)=NULL;

        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
            update_inode_colors(box,child,&child_color);
            if (child_color) {
				if (!(*color)) {
					(*color)=AllocBitTab();
					CopyBitTab(color,child_color);
				} else fusionneBitTab(color,child_color);
			}
        }
		if (*color) {
			inodes_color[box][node]=(*color);
			nb_inodes_color[box][node]=nbSequenceInBitTab(*color);
		}
	}
}

/*===========================================================*/
void update_inode_tree(unsigned short box)
/*===========================================================*/
{
	update_inode_colors(box,0,&inodes_color[box][0]);
}

#ifdef STATIC_BOX_LINKS
/*===========================================================*/
void free_tree(unsigned short box, unsigned int node)
/*===========================================================*/
{
	/* assert(box==1); */
    if (is_leaf(node)) {
		unsigned int leaf_index=su_leaf_master_index[(node &~LEAF_BIT)];
		if (leaves_color[box][leaf_index]) {
			free(leaves_color[box][leaf_index]);
			leaves_color[box][leaf_index]=NULL;
			nb_leaves_color[box][leaf_index]=0;
		}
	} else {
		if (nb_inodes_color[box][node]) {
			unsigned int child;
			for (child=su_get_first_child(node);child;child=su_get_sibling(child))
				free_tree(box,child);
			free(inodes_color[box][node]); 
			inodes_color[box][node]=NULL;
			nb_inodes_color[box][node]=0;
		}
    }
}
#endif /* #ifdef STATIC_BOX_LINKS */

#ifndef STATIC_BOX_LINKS
/*===========================================================*/
void free_middle_box_tree(unsigned short box, unsigned int node)
/*===========================================================*/
{
    if (is_leaf(node)) {
		unsigned int leaf_index=su_leaf_master_index[(node &~LEAF_BIT)];
		if (leaves_color[box][leaf_index]) {
			free(leaves_color[box][leaf_index]);
			leaves_color[box][leaf_index]=NULL;
			nb_leaves_color[box][leaf_index]=0;
			free_leaf_list(reached_leaves[box-1][leaf_index]);
			reached_leaves[box-1][leaf_index]=NULL;
		}
	} else {
		if (nb_inodes_color[box][node]) {
			unsigned int child;
			for (child=su_get_first_child(node);child;child=su_get_sibling(child))
				free_middle_box_tree(box,child);
			free(inodes_color[box][node]); 
			inodes_color[box][node]=NULL;
			nb_inodes_color[box][node]=0;
		}
    }
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void free_last_leaf_tree(unsigned short box, unsigned int node)
/*===========================================================*/
{
    if (is_leaf(node)) {
		unsigned int leaf_index=su_leaf_master_index[(node &~LEAF_BIT)];
		if (leaves_color[box][leaf_index]) {
			free(leaves_color[box][leaf_index]);
			leaves_color[box][leaf_index]=NULL;
			nb_leaves_color[box][leaf_index]=0;
		}
	} else {
		if (nb_inodes_color[box][node]) {
			unsigned int child;
			for (child=su_get_first_child(node);child;child=su_get_sibling(child))
				free_last_leaf_tree(box,child);
			free(inodes_color[box][node]); 
			inodes_color[box][node]=NULL;
			nb_inodes_color[box][node]=0;
		}
    }
} /* #ifndef STATIC_BOX_LINKS */
/*===========================================================*/
void free_tree(unsigned short box, unsigned int node)
/*===========================================================*/
{
	/*assert(box!=0);*/
	if (box+1<params.p) 
		/* Optimized for middle box of an extraction */
		free_middle_box_tree(box,node);
	else {
		/* Optimized for last box of an extraction */
		free_last_leaf_tree(box,node);
#ifdef SMILE_OUTPUT
		reset_occurrences();
#endif /* #ifdef SMILE_OUTPUT */
	}
}
#endif /* #ifndef STATIC_BOX_LINKS */


/****************************************************************/
/****************************************************************/
/*			Backwards compatibility with SMILE output			*/
/****************************************************************/
/****************************************************************/


#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
/*===========================================================*/
void init_occurrences()
/*===========================================================*/
{
	int i;

	nb_leaves_occ=(unsigned int*)malloc(sizeof(unsigned int)*(master_leaves_created+1));
	memset((void *)nb_leaves_occ,0,sizeof(unsigned int)*(master_leaves_created+1));	

	/* Case of one box only */
	if (params.p==1) {
		for (i=0;i<=last_leaf_created;i++)
			nb_leaves_occ[su_leaf_master_index[i]]++;
	}
} /* #ifndef STATIC_BOX_LINKS && #ifdef SMILE_OUTPUT */
/*===========================================================*/
void reset_occurrences()
/*===========================================================*/
{
	memset((void *)nb_leaves_occ,0,sizeof(unsigned int)*(master_leaves_created+1));	
} /* #ifndef STATIC_BOX_LINKS && #ifdef SMILE_OUTPUT */
/*===========================================================*/
unsigned int get_occurrences(unsigned int node)
/*===========================================================*/
{
	if (is_leaf(node))
		return nb_leaves_occ[su_leaf_master_index[node&~LEAF_BIT]];
	else {
        unsigned int child, nb_occ=0;
        for (child=su_get_first_child(node);child;child=su_get_sibling(child)) {
			nb_occ+=get_occurrences(child);
		}
		return nb_occ;
	}
} /* #ifndef STATIC_BOX_LINKS && #ifdef SMILE_OUTPUT */
/*===========================================================*/
void clean_occurrences()
/*===========================================================*/
{
	free(nb_leaves_occ);
}
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

