#ifndef _GLOBAL_CONSTANTS_H
#define _GLOBAL_CONSTANTS_H

#include"global_types.h"


extern struct parameters params;

/* Fasta variables */
extern char *in_alphabet;
extern FILE *output_file;
extern char *params_out_file;
extern unsigned int delta_k; /* k_max[0]-k_min[0]+1 */
extern unsigned int *alpha_mask; /* LIMITATION: This unsigned int is limiting alphabet size to 32 */
extern unsigned short alpha_size;

#ifndef STATIC_BOX_LINKS
#ifdef SMILE_OUTPUT
extern unsigned int extracted_k_max;
#endif /* #ifdef SMILE_OUTPUT */
#endif /* #ifndef STATIC_BOX_LINKS */

#endif




