#ifndef _LEAF_LIST_H
#define _LEAF_LIST_H


typedef struct leaf_elem_struct {
    unsigned int leaf;
    struct leaf_elem_struct* prox;
} leaf_elem;

typedef struct leaf_elem_struct *leaf_list;

/* To call add_leaf just declare a leaf_list and initialize it to NULL */
void add_leaf(leaf_list* l, unsigned int e);
void free_leaf_list(leaf_list l);

#endif
