#ifndef _OCC_LIST_H
#define _OCC_LIST_H


struct node_occ {
    unsigned int node;
    unsigned int err;
    unsigned int depth;
    unsigned int forward; /* Depth spelled for the correspondent node (for arcs with more than one label) */
    unsigned int child;
};

typedef struct node_occ_elem_struct {
    struct node_occ* nocc;
    struct node_occ_elem_struct* prox;
} node_occ_elem;

typedef struct node_occ_elem_struct *occ_list;

/* To call add_node_occ just declare an occ_list and initialize it to NULL */
void add_node_occ(occ_list* l, struct node_occ* e);
void free_node_occ_list(occ_list l);

#endif
