#include<malloc.h>
#include<stdio.h>
#include<stdlib.h>
#include"leaf_list.h"


#define ERR_MALLOC "error: malloc"

/* =========================================================== */
void quit_leaf_list(char* str, int status)
/* =========================================================== */
{
    printf("%s\n",str);
    exit(status);
}

/* =========================================================== */
void add_leaf(leaf_list* l, unsigned int e)
/* =========================================================== */
{
    leaf_elem *n = (leaf_elem*) malloc(sizeof(struct leaf_elem_struct));
    if (!n) quit_leaf_list(ERR_MALLOC, 1);
    n->leaf = e;
    n->prox = *l;
    *l = n;
}

/* =========================================================== */
void free_leaf_list(leaf_list l)
/* =========================================================== */
{
    leaf_elem *e;
    while (l) {
        e = l;
        l = l->prox;
        free(e); e=NULL;
    }
    l=NULL;
}
