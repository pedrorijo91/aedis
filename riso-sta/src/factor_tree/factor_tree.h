#ifndef _FACTOR_TREE_H
#define _FACTOR_TREE_H

#include"bit_tab.h"

void build_gen_factor_tree();
void clean_gen_factor_tree();

void build_colors();
void clean_colors();

/* Factor Tree API to export (if needed) */
/*
int is_leaf(unsigned int node);
unsigned int get_first_child(unsigned int inode);
unsigned int get_sibling(unsigned int node);
unsigned int get_inode_depth(unsigned int inode);
unsigned int get_depth(unsigned int node);
unsigned int get_head(unsigned int node);
Bit_Tab *get_color(unsigned int node);
unsigned int get_nb_colors(unsigned int node);
*/

#endif