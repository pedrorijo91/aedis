#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>
#include<math.h>
#include"fasta.h"

/* Fasta variables */
char *in_str;
unsigned int N;
unsigned int k;
unsigned int *offset;
unsigned int num_leaves;


/*====================================*/
int init_fasta_parser(FILE *fp)
/*====================================*/
{
	char c;

	while (((c = getc(fp)) != '>' ) && (c != EOF ));
	if (c == EOF) return 1;
	return 0;
}

/*====================================*/
int ignore_fasta_comment(FILE *fp)
/*====================================*/
{
	char c;

	while (((c = getc(fp)) != '\n' ) && (c != EOF ));
	if (c==EOF) return 1;
	return 0;
}

/*====================================*/
int count_fasta(FILE *fp)
/*====================================*/
{
	char c;

	N++;
	while (((c = getc(fp)) != '>' ) && (c != EOF )){
		if (c!='\n' && c!='\r') num_leaves++;  
	}
	if (c == EOF) return 1;
	return 0;
}


/*====================================*/
void count_in_str(char *path)
/*====================================*/
{
	FILE *fp;

	num_leaves=0;
	N=0;
	if ((fp = fopen(path, "r"))==NULL) {
		printf("Error: cannot open FASTA file %s\n",path);
		exit(1);
	} else {
		if (init_fasta_parser(fp)){
			printf("Error: %s is not a FASTA file\n",path);
		    exit(1);
		}
		if (ignore_fasta_comment(fp)){
			printf("Error: %s is a bad FASTA file\n",path);
		    exit(1);
		}
		while (!count_fasta(fp)) {
			if (ignore_fasta_comment(fp)){
				printf("Error: %s is a bad FASTA file\n",path);
				exit(1);
			}
		}
		fclose(fp);

		/*N accounts for the terminator of each input string*/
		num_leaves+=N;
	}
}

/*====================================*/
int read_fasta(FILE *fp, int *head)
/*====================================*/
{
	char c;

	while (((c = getc(fp)) != '>' ) && (c != EOF )){
		if (c!='\n' && c!='\r') {
			c=toupper(c);
			in_str[*head]=c;
			(*head)++;
		}
	}
    in_str[*head]='_';
	(*head)++;
	if (c == EOF) return 1;
	return 0;
}

/*====================================*/
void set_in_str(char *path)
/*====================================*/
{
	FILE *fp;
	int head=0;
	int off_swap=0;
    
    if ((fp = fopen(path, "r"))==NULL) {
		printf("Error: cannot open FASTA file %s\n",path);
		exit(1);
	} else {
		if (init_fasta_parser(fp)){
			printf("Error: %s is not a FASTA file\n",path);
		    exit(1);
		}
		if (ignore_fasta_comment(fp)){
			printf("Error: %s is a bad FASTA file\n",path);
		    exit(1);
		}

		offset=(unsigned int *)malloc((N+1)*sizeof(unsigned int));
		offset[off_swap]=0;
		off_swap++;
		while (!read_fasta(fp,&head)) {
			offset[off_swap]=head;
			off_swap++;
			if (ignore_fasta_comment(fp)){
				printf("Error: %s is a bad FASTA file\n",path);
				exit(1);
			}
		}
		fclose(fp);
		offset[N]=head;
		in_str[head]=0;
	}
}

/*====================================*/
void get_fasta(char *path)
/*====================================*/
{
	count_in_str(path);
	in_str=(char *)malloc((num_leaves+1)*sizeof(char));
	set_in_str(path);
}

/*====================================*/
void clean_fasta()
/*====================================*/
{
	free(offset);
	free(in_str);
}

