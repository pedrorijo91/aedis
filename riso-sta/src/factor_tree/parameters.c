#include<stdio.h>
#include<stdlib.h>
#include"fasta.h"
#include"parameters.h"


/*====================================*/
void check_parameters(char *fasta)
/*====================================*/
{
	printf("===============================================================\n");
	printf("Factor tree is going to be built with the following parameters:\n");
	printf("===============================================================\n\n");
	printf("FASTA file          %s\n",fasta);
	printf("Factor tree depth   %d\n",k);
}

/*====================================*/
void get_params(char **argv)
/*====================================*/
{
	/* Reading fasta file to initialize in_srt */
	get_fasta(argv[1]);

	/* Reading factor tree depth */ 
	k=atoi(argv[2]);

	check_parameters(argv[1]);
}

/*====================================*/
void clean_params()
/*====================================*/
{
	clean_fasta();
}