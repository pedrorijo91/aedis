#include<stdio.h>
#include<stdlib.h>
#include"fasta.h"
#include"parameters.h"
#include"factor_tree.h"
#ifdef LINUX_CPU_TIME
#include"linux_cpu_time.h"
#endif /* #ifdef LINUX_CPU_TIME */

#ifndef LINUX_CPU_TIME
float print_cpu_time(char flag) { return (float)0; }
#endif /* #ifndef LINUX_CPU_TIME */

/*====================================*/
int main(int argc, char *argv[])
/*====================================*/
{
#ifndef LINUX_CPU_TIME
	unsigned int i;
#endif /* #ifndef LINUX_CPU_TIME */
	float t=0.0,t_factor_tree=0.0,t_colors=0.0;

	/* Get params */
	print_cpu_time(1);
	get_params(argv);
	t=print_cpu_time(0);

	/* Build generalized factor tree */
	print_cpu_time(1);
	build_gen_factor_tree();
	t_factor_tree=print_cpu_time(0);
	printf("%d-factor tree built in %.2f seconds\n",k,t_factor_tree);

	/* Build colors of the generalized factor tree */
	print_cpu_time(1);
	build_colors();
	t_colors=print_cpu_time(0);
	printf("%d-factor tree colors built in %.2f seconds\n",k,t_colors);

	/* Cleanup everything and computing total time spent */
	print_cpu_time(1);
	clean_colors();
	clean_gen_factor_tree();
	clean_params();
	t=t+print_cpu_time(0);
    printf("total CPU time: %.2f\n",(double)(t+t_factor_tree+t_colors));
#ifndef LINUX_CPU_TIME
	scanf("%d",&i);
#endif /* #ifndef LINUX_CPU_TIME */
	exit(0);
}


