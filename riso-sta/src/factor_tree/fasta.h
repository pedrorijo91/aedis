#ifndef _FASTA_H
#define _FASTA_H

/* Fasta variables */
extern char *in_str;
extern unsigned int N;
extern unsigned int k;
extern unsigned int *offset;
extern unsigned int num_leaves;

void get_fasta(char *path);
void clean_fasta();

#endif
