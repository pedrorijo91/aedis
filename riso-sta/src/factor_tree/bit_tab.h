#ifndef _BIT_TAB_H
#define _BIT_TAB_H

#include<string.h>
#include"struct_tab.h"

void initBitTab(int nb_seq);
Bit_Tab *AllocBitTab(void);

void addBitTabValue(Bit_Tab **tab,int value);
void fusionneBitTab(Bit_Tab **tab1,Bit_Tab *tab2); /* 1 <- 1 & 2 */
int  nbSequenceInBitTab(Bit_Tab *tab);

void printBitTab(Bit_Tab *tab);
void CopyBitTab(Bit_Tab **dest,Bit_Tab *src);
void ReinitBitTab(Bit_Tab **bt);
/*---------------------------------------------------------*/

void convertBitTab(Bit_Tab **tab); /* transforme tab de dyn a static... */

void addBitTabValueStatic(Bit_Tab **tab,int value);
void addBitTabValueDynamic(Bit_Tab **tab,int value);

int  nbSequenceInBitTabStatic(Bit_Tab *tab);
int  nbSequenceInBitTabDynamic(Bit_Tab *tab);

void fusionneBitTabStatic(Bit_Tab **tab1,Bit_Tab *tab2);
void fusionneBitTabDynamic(Bit_Tab **tab1,Bit_Tab *tab2);
Bit_Tab *AllocBitTabStatic(void);

#if DEBUG_JTREE
extern int nb_alloc_tab;
#endif
#endif 

