#include<iostream>
#include <stdlib.h>
#include <cstring>
#include <vector>

using namespace std;

/********** declaracao de variaveis globais **********/
string T;
/********** fim de variaveis globais **********/

void naiveAlgorithm(string pattern);

void kmpAlgorithm(string pattern);
vector<int> computePrefixFunction(string pattern);

void bmAlgorithm(string pattern);
void computeR(string pattern, int* r, size_t size);
void computeL(string pattern, int* L, int* l);
void zAlgorithm(string pattern, int* z, int* ll);

/********** funcoes auxiliares **********/
void saveVar(string type, string content) {

	if(type.compare("T") == 0) {
		T = content;
	}

	if(type.compare("N") == 0) {
		naiveAlgorithm(content);
	}

	if(type.compare("K") == 0) {
		kmpAlgorithm(content);
	}

	if(type.compare("B") == 0) {
		bmAlgorithm(content);
	}
}

void readInput() {

	string type;
	string content = "";

	cin >> type;

	while(type != "X") {

		cin >> content;

		saveVar(type, content);

		cin >> type;
	}
}

/********** fim funcoes auxiliares **********/

/********** inicio dos algortimos **********/

/*#### inicio naive ####*/
void naiveAlgorithm(string pattern) {

	size_t i = 0;
	size_t j = 0;

	long comps = 0;

	for(i = 0; i <= T.length() - pattern.length(); i++) {
		bool founded = true;

		for(j = 0; j < pattern.length(); j++) {
			comps++;
			if(T[i+j] != pattern[j]) {
				founded = false;
				break;
			}
		}

		if(founded) {
			cout << i << " ";
		}
	}
	cout << endl;
		cerr << "comps: " << comps << endl;
}
/*#### fim naive ####*/

/*#### incio kmp ####*/
void kmpAlgorithm(string pattern) {

	size_t n = T.length();
	size_t m = pattern.length();
	vector<int> pi = computePrefixFunction(pattern);

	size_t i = 0;
	size_t q = 0;
	long comps = 0;

	while(i < n) {
		if(pattern[q] == T[i]) {
			comps++;
			if(q == m-1) {
				cout << i - (m - 1) << " ";
			}
			i++;
			q++;
		}
		else {
			comps++;
			if(q > 0) {
				q = pi[q-1];
			}
			else {
				i++;
			}
		}
	}

	cout << endl << comps << " " << endl;
	cerr << endl << comps << " " << endl;
}

vector<int> computePrefixFunction(string pattern) {
	size_t m = pattern.length();

	vector<int> pi = vector<int> (pattern.length());
	pi[0] = 0;

	int k = 0;
	for(size_t q = 2; q <= m; q++) {
		while(k > 0 and pattern[k] != pattern[q-1]) {
			k = pi[k-1];
		}
		if(pattern[k] == pattern[q-1]) {
			k++;
		}
		pi[q-1] = k;
	}

	return pi;
}
/*#### fim kmp ####*/

/*#### incio bm ####*/
void bmAlgorithm(string pattern) {

	size_t m = T.length();
	size_t n = pattern.length();

	if(n > m) {
		cout << " " << endl << 0 << endl;
		return;
	}

	/********* PRE-PROCESSING STAGE ********/
	int r [4] = {0, 0, 0, 0 }; 
	computeR(pattern, r, 4);

	int* L = (int*) malloc(m*sizeof(int));
	int* l = (int*) malloc(m*sizeof(int));
	memset(L,0,m);
	memset(l,0,m);

	for(size_t i = 0; i < m; i++) {
		L[i] = 0;
		l[i] = 0;
	}
	computeL(pattern, L, l);

	/********* SEARCH STAGE ********/
	long comps = 0;
	size_t k = n;
	while(k <= m) {
		int i = n;
		int h = k;
		//		cout << " h: " << h << " e pattern vs T comp: " << pattern[i-1] << " vs " << T[h-1] << endl;
		//		cout << "P : " << pattern << endl;
		//		cout << "T : " << T[h-1-n+1]  << T[h-1-n+2] << T[h-1-n+3] << T[h-1-n+4]<< endl;
		while(i > 0 and pattern[i-1] == T[h-1]) {
			//		cout << "entrei no while" << endl;
			i--;
			h--;
			comps++;
		}
		if(i == 0) {
			//			cout << "FOUND IN BM !!!!!!!!!!!!!!!!! " << endl;
			cout << h << " ";
			//			cout << "k antes " << k << endl;
			//			cout << "l'(1) " << l[1] << endl;
			int lAdvance = l[0];
			if(n > 1) {
				lAdvance = l[1];
			}
			k = k + n - lAdvance;
			//			cout << "k depois de find " << k << endl;
		}
		else {
			comps++;
			int rt_k = 0;
			if(T[h-1] == 'A') {
				rt_k = r[0];
				//				cout << "rt_k de A " << rt_k << endl;
			}
			else {
				if(T[h-1] == 'C') {
					rt_k = r[1];
					//					cout << "rt_k de C " << rt_k << endl;
				}
				else {
					if(T[h-1] == 'G') {
						rt_k = r[2];
						//						cout << "rt_k de G " << rt_k << endl;
					}
					else {
						if(T[h-1] == 'T') {
							rt_k = r[3];
							//						cout << "rt_k de T " << rt_k << endl;
						}
					}
				}
			}
			//			cout << "badchar: rt_k = " << rt_k << endl;
			int badChar = max(1, i - rt_k);

//			cout << "indice=" << i << " L l " << L[i] << " " << l[i] << endl;
			int goodSuffix = 1; // * -1 ?? * //
			if(((size_t) i) < n) {
				if(L[i] == -1) {
					if(((int) i) == ((int) n) /*&& l[i-1] == 0*/) {
//						cout << "i = n" << endl;
						goodSuffix = 1;
					}
					else {
//						cout << "estou a usar l(i)" << endl;
						goodSuffix = n - l[i]; // l[i] porque so queremos a parte que fez match
					}
				}
				else {
//					cout << "usei o L(i)" << endl;
					goodSuffix = n - L[i];
				}
			}
			else {
				//	cout << "i=n " << i << " " << n << endl;
			}
//			cout << "badChar and goodSuffix " << badChar << " and " << goodSuffix << endl;
			//			cout << "k before: " << k << endl;
			k += max(badChar,goodSuffix); 
			//cout << "k after: " << k << endl;
		}
	}
	cout << endl << comps << " " << endl;
	cerr << endl << comps << " " << endl;
	free(L);
	free(l);
}

void computeR(string pattern, int* r, size_t size) {
	// bad char rule
	// A C G T
	//R(x) indica ocorrencia mais a direita de cada letra no padrao
	size_t m = pattern.length();

	//usar contador
	//usar inteiros/hash em vez de array
	for(size_t i = 0; i < m; i++) {
		if(pattern[i] == 'A') {
			r[0] = i+1;
		}
		else {
			if(pattern[i] == 'C') {
				r[1] = i+1;
			}
			else{
				if(pattern[i] == 'G') {
					r[2] = i+1;
				}
				else {
					if(pattern[i] == 'T') {
						r[3] = i+1;
					}
				}
			}	
		}
	}

	//	for(int i = 0; i < 4; i++) {
	//		cout << "R[i], i=" << i << " " << r[i] << endl;
	//	}
}

void computeL(string pattern, int* L, int* l) {
	//calcular N: executar Z em P revertido
	//Nj[] = zAlgorithm(reversePattern);
	// L(i) =  maior posicao inferior a 'm' tal que P[i,...,m] matches sufixo de P[1,...L(i)]
	//L[] = L'(i) =  maior posicao inferior a 'm' tal que  P[i,...,m] matches sufixo de P[1,...,m] e o caracter que precede o sufixo e diferente de P[i-1]
	//l[] = l(i)' = comprimento do maior sufixo de P[i,...,m] que tambem e prefixo de P.
	string reversePattern = string (pattern.rbegin(), pattern.rend());
	size_t m = reversePattern.length();
	//	int Nj[m];
	int* Nj = (int*) malloc(m*sizeof(int));
	memset(Nj,0,m);

	int* z = (int*) malloc(m*sizeof(int));
	memset(z,0,m);

	for(size_t i = 0; i < m; i++) {
		L[i] = -1;
		l[i] = 0;
		Nj[i] = 0;
		z[i] = 0;
	}

	zAlgorithm(reversePattern, z, l);

	for(size_t i = 0; i < m; i++) {
		Nj[i] =  z[m - 1 - i];
	}

	for(size_t j = 1;  j <= m; j++) {
		int i = m - Nj[j-1] + 1;
		L[i-1] = j;
	} 


	//pedrada a calcular l'(i)
	for(size_t i = 0; i < m; i++) {
		string sub = pattern.substr(i);
		size_t subSize = sub.length();

		for(size_t j = 0; j < subSize; j++) {
			if(sub[subSize-1-j] == pattern[j]) {
				l[i] = j+1;
			}
			else {
				break;
			}
		}
	}


	/*
	for(size_t x = 0; x < m; x++) {
		cout << "Nj de " << x << " = " << Nj[x] << endl;
	}
	cout << endl;

	for(size_t i = 0; i < m; i++) {
		cout << "L'(i),i=" << i << " " << L[i] << endl;
	}
	cout << endl;

	for(size_t i = 0; i < m; i++) {
		cout << "l'(i),i=" << i << " " << l[i] << endl;
	}
	cout << endl;
	*/

	free(Nj);
}

void zAlgorithm(string pattern, int* z, int* ll) { 

	int m = pattern.length();
	z[0] = 0;

	int r = 0;
	int l = 0;
	int k = 1;

	string s1 = pattern;
	string sk = pattern.substr(k);

	for(int i = 1; i < m; i++) {
		//Case 1: k > r
		if(i >= r) {
			//	cout << "DEBUG. caso 1 com k=" << i+1 << endl;
			l = r = i;
			while(r < m && pattern[r-l] == pattern[r]) {
				r++;
			}
			z[i] = r-l;
			r--;
		}
		else {
			//Case 2: k<= r
			k = i - l;
			//	cout << "k' = " << k << " e box: " <<  r-i+1 << endl;
			if(z[k] < r - i + 1) {
				//Case 2a: Zk' < |B|
				//	cout << "DEBUG. caso 2a com k l r " << i+1 << " " << l+1 << " " << r+1 << endl;
				z[i] = z[k];
			}
			else {
				//Case 2b: Zk' >= |B|
				//	cout << "DEBUG. caso 2b com k=" << i+1 << endl;
				l = i;
				while(r < m && pattern[r-l] == pattern[r]) {
					r++;
				}
				z[i] = r - l;
				r--;
			}
		}

		//		cout << "Z algo: i r m " << i << " " << r << " " << m << endl;
		if(r == m-1) {
			ll[i] = r-i+1;
		}
	}

	for(size_t i = 0; i < pattern.length(); i++) {
		//	cout << "Z de " << i+1 << " ( " << pattern.substr(i) << " ): " << z[i] << endl;
	}
}
/*#### final bm ####*/

/********** fim dos algortimos **********/


/********** main **********/
int main(){

	readInput();
	return 0;
}
