#include<iostream>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <assert.h>  
#include <stack>
#include <stdio.h>

using namespace std;

/********** declaracao de estruturas **********/
typedef struct node* nodep;
struct node {
	int id; //DEBUG
	int Ti; //id da string 
	int head; //primeiro caracter de patterns[Ti] a sair da root
	int sdep;
	nodep child;
	nodep brother; 
	nodep slink;
	nodep* hook;
};

struct point {
	nodep a;
	nodep b;
	int s; 
};
typedef struct point* pointp; 
/********** fim de estruturas **********/

/********** declaracao de funcoes **********/
void createRoot();
/********** fim de declaracao de funcoes **********/

/********** declaracao de variaveis globais **********/
string* patterns;
size_t* sizes;
int nPatterns = 0;
int nodeCounter = 1;

nodep rootp;
nodep slRootp;

bool** Lv;

int** results;
/********** fim de variaveis globais **********/

/********** funcoes debug **********/

int getNodeId() {
	int id = nodeCounter;
	nodeCounter++;
	return id;
}

void printNodep(nodep n) {
	cerr << "===================" << endl;
	cerr << "printing node" << endl;
	if(n != NULL) {
		cerr << "node ID: " << n->id << endl;
		cerr << "node Ti: " << n->Ti << endl;
		cerr << "node head: " << n->head << endl;
		cerr << "node sdep: " << n->sdep << endl;

		cerr << "node child: ";
		if(n->child != NULL) {
			cerr << n->child->id;
			cerr << " @ mem pos: " << n->child << endl;
		}
		else{
			cerr << "NULL" << endl;
		}

		cerr << "node brother: ";
		if(n->brother != NULL) {
			cerr << n->brother->id;
			cerr << " @ mem pos: " << n->brother << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node slink: ";
		if(n->slink != NULL) {
			cerr << n->slink->id << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node hook: ";
		assert(n->hook != NULL);
		// assert(*(n->hook) != NULL);
		if(n->hook != NULL && *(n->hook) != NULL) {
			cerr << (*(n->hook))->id;
			cerr << " @ mem pos: " << *(n->hook) << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
	}
	else {
		cerr << "node is NULL" << endl;
	}
	cerr << "finished printing node" << endl;
	cerr << "===================" << endl;
}

void printPointp(pointp p) {
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
	cerr << "printing point" << endl;

	cerr << "printing node above:" << endl;
	printNodep(p->a);
	cerr << "finished printing node above" << endl;

	cerr << "printing node bellow:" << endl;
	printNodep(p->b);
	cerr << "finished printing node bellow:" << endl;

	cerr << "point sdep: " << p->s << endl;

	cerr << "finished printing point" << endl;
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
}

void dfs(nodep p) {

	printNodep(p);
	if(p->child != NULL) {
		dfs(p->child);
	}
	else {
		cerr << "node without child" << endl;
	}
	if(p->brother != NULL) {
		dfs(p->brother);
	}
	else {
		cerr << "node without brother" << endl;
	}
}

void printTree(nodep root) {
	cerr << endl << endl << "PRINTING TREE" << endl;
	//realizar DFS
	printNodep(root);
	if(root->child != NULL) {
		dfs(root->child);
	}
	cerr <<"FINISHED TREE" << endl << endl; 
}

void printLv() {

	for(int i = 0; i < nodeCounter; i++) {
		for(int j = 0; j < nPatterns; j++) {
			cout << "Lv [" << i << "][" << j << "] : " << Lv[i][j] << endl;
		}
	}
}

/********** fim de funcoes debug **********/

/********** funcoes auxiliares **********/

void readInput() {

	cin >> nPatterns;

	patterns = new string[nPatterns]; 
	sizes = new size_t[nPatterns]; 

	string m;
	string pattern;
	for(int i = 0; i < nPatterns; i++) {
		patterns[i] = ""; 

		cin >> m;
		cin >> pattern; 

		patterns[i] = pattern + "$";
		sizes[i] = atoi(m.c_str()) + 1;
	}

	results = (int**)malloc(nPatterns * sizeof(int*));

	for (int i = 0; i < nPatterns; i++) {
		results[i] = (int*)malloc(nPatterns * sizeof(int));
		for(int j = 0; j < nPatterns; j++) {
			results[i][j] = 0;
		}
	}


	//DEBUG
	// for(int i = 0; i < nPatterns; i++) {
	// 	cerr << "READ INPUT - string " << i << ": " << patterns[i] << endl;
	// }
}

void createRoot() {
	slRootp = (struct node*) malloc(sizeof(struct node));

	slRootp->id = -1;
	slRootp->Ti = 0;
	slRootp->head = 0;
	slRootp->sdep = -1;
	slRootp->child = rootp;
	slRootp->brother = NULL;
	slRootp->slink = slRootp;
	slRootp->hook = &(slRootp->slink);	

	rootp = (struct node*) malloc(sizeof(struct node));

	rootp->id = 0;
	rootp->Ti = 0;
	rootp->head = 0;
	rootp->sdep = 0;
	rootp->child = NULL;
	rootp->brother = NULL;
	rootp->slink = slRootp;
	rootp->hook = &(slRootp->child);
}

struct point createPoint(nodep nodeA) {
	struct point p;
	p.s = nodeA->sdep;
	p.a = nodeA;
	p.b = nodeA->child;

	return p;
}

bool descendQ(pointp p, char c) {

	if(p->a == slRootp) {
		// cerr << "descendQ from slRootp => true" << endl;
		return true;
	}

	if(p->b == NULL) {
		// cerr << "descendQ: p->b is NULL. p->a=" << p->a->id << endl;
		return false;
	}

	nodep n = p->a;

	if(n->sdep == p->s) { //se esta num node tem de testar os varios filhos
		// cerr << "descendQ: estou num node: " << n->id << endl;
		nodep brother = n->child;
		while(brother != NULL) {
			// cerr << "descendQ: testing in descent to node: " << brother->id << " - chars: " << c << " and " << patterns[brother->Ti][brother->head + p->s] << endl;
			if(patterns[brother->Ti][brother->head + p->s] == c) {
				return true;
			}
			brother = brother->brother;
		}
		return false;
	}
	else {
		// cerr << "descendQ: nao estou num node: " << n->id << " and " << p->b->id << " p->s:" << p->s << ". comp: " << patterns[p->b->Ti][p->b->head + p->s] << " and " << c << endl;
		return (patterns[p->b->Ti][p->b->head + p->s] == c);
	}
}

void descend(pointp p, char c) {

	if(p->a == slRootp) {
		p->a = rootp;
		p->b = p->a->child;
		p->s = 0;
		return;
	}

	if(p->s == p->a->sdep) { //se esta em node verificar qual filho desce
		// cerr << "descend: estou num node " << p->a->id << " looking for " << c << endl;
		nodep n = p->b;
		while(n != NULL) {
			// cerr << " testar direccao " << n->id << endl;
			if(patterns[n->Ti][n->head + p->s] == c) {
				// cerr << "encontrei caminho em direccao a " << n->id << endl;
				p->b = n;
				break;
			}
			n = n->brother;
		}
	}

	p->s++;

	if(p->s == p->b->sdep) { //se quando descer chega ao node above
		p->a = p->b;
		p->b = p->b->child;
	}
}

void descendSkip(pointp p, char c) {

	assert(p->a->id != (p->b != NULL ? p->b->id : -2));

	if(p->a == slRootp) {
		// cerr << "descendSkip: estou na slRootp" << endl;
		p->a = rootp;
		p->b = p->a->child;
		p->s = 0;
	}
	else {
		// cerr << "descendSkip: NAOO estou na slRootp: " << p->a->id << endl;
		nodep n = p->b;
		while(n != NULL) {
			if(patterns[n->Ti][n->head + p->s] == c) {
				p->a = n;
				p->b = n->child;
				p->s = n->sdep;
				// cerr << "descendSkip: encontrei descida !!!!!" << endl;
				break;
			}
			// cerr << "muda de brother" << endl;
			n = n->brother;
		}
	}
}

bool existsSL(nodep n, pointp p) {
	nodep sl = p->a->slink;

	int pattern = n->Ti;

	struct point point = createPoint(sl);
	pointp slp = &point;

	int slHead = n->head + 1;

	while(slp->s < (n->sdep - 1) && descendQ(slp, patterns[pattern][slHead + slp->s])) {
		descendSkip(slp,patterns[pattern][slHead + slp->s]);
	}

	if(slp->s == (n->sdep - 1)) {
		n->slink = slp->a;
		return true;
	}

	return false;
}

nodep createNode(pointp p, int si, int head) {
	nodep leafParent = p->a;

	if(p->s > p->a->sdep) { //se estou no meio de nodes tenho de criar no em p
		struct node* n = (struct node*) malloc (sizeof *n);
		n->id = getNodeId();
		n->Ti = si;
		n->head = head;
		n->sdep = p->s; 
		n->child = p->b;
		n->hook = &(p->a->child);
		n->slink = NULL;

		{ //SLink block: assigns SL for current node and some waiting node
			if(slRootp->slink != slRootp) { //rootp contem node em espera que o seu SL seja criado
				slRootp->slink->slink = n;
				slRootp->slink = slRootp;
			}
			if(!existsSL(n,p)) { //se SL nao existe guarda em espera na rootp
				slRootp->slink = n;
			}
		}

		if(p->b != p->a->child) {
			// cerr << "createNode: p->b NOT child directo de p->a. p->a->child=" << p->a->child->id << endl;
			n->brother = p->a->child;

			if(n->child != NULL) {
				*(n->child->hook) = n->child->brother;

				if(n->child->brother != NULL) {
					(n->child->brother->hook) = (n->child->hook);
				}

				n->child->brother = NULL; 

			}


			if(n->brother != NULL) {
				// cerr << "n->brother NOT NULL: " << n->brother->id << endl;
				n->brother->hook = &n->brother;
			}

		}
		else {
			// cerr << "createNode: p->b e child directo de p->a" << endl;
			// printNodep(n);
			n->brother = n->child->brother;

			if(n->brother != NULL) {
				// cerr << "n brother not null: " << n->brother->id << endl;
				n->brother->hook = &(n->brother);
				// printNodep(n->brother);
			}

			n->child->brother = NULL;

			*(n->child->hook) = n->child;

		}
		p->a->child = n;

		leafParent = n;

		// cerr << "createNode: print do novo node: " << endl;
		// cerr << "createNode: n->id " << n->id << " n-child: " << n->child->id << " n->brother: " << (n->brother != NULL ?  n->brother->id : -100) << " n->brother->brother: " << (n->brother != NULL && n->brother->brother != NULL ?  n->brother->brother->id : -100) << " n->hook: " << (n->hook != NULL  ?  (*(n->hook))->id : -100) << endl;
	}

	//criar folha
	struct node* leaf = (struct node*) malloc (sizeof *leaf);	
	leaf->id = getNodeId();
	leaf->Ti = si;
	leaf->head = head;
	leaf->sdep = patterns[si].length() - head;
	leaf->child = NULL;
	leaf->slink = NULL;

	leaf->hook = &(leafParent->child);
	leaf->brother = *(leaf->hook);

	*(leaf->hook)=leaf;

	if(p->b != NULL) {
		p->b->hook = &(leaf->brother);
	}

	leafParent->child = leaf;

	assert(*(leaf->hook)==leaf);

	// cerr << "createNode: leaf->id " << leaf->id << " n->brother: " << (leaf->brother != NULL ?  leaf->brother->id : -100) << endl;

	return leafParent;
}

void suffixJump(pointp p, nodep node, int pattern, int j) { 
	 // #warning "skip count trick"

	if(node->slink != NULL) {
		// cerr << "node has slink to" << node->slink->id << endl;
		p->a = node->slink;
		p->b = p->a->child;
		p->s = p->a->sdep;
		return;
	}
	else {
		int head = node->child->head;
		nodep slpa = p->a->slink;
		p->a = slpa;
		p->b = slpa->child; //possivelmente vazia
		p->s = slpa->sdep;

		// if(slpa == slRootp) {
		// 	cerr << "sl de p->a is slRootp" << endl;
		// 	while(p->s < (node->sdep -1)) {
		// 		descend(p, patterns[pattern][head + p->s]);
		// 		//no final acaba em node'
		// 	}
		// }
		// else {
			// cerr << "sl de p->a is node " << slpa->id << endl;
			while(p->s < (node->sdep -1)) {
				descend(p, patterns[pattern][head + 1 + p->s]);
				//no final acaba em node'
			}
		// }
	}

}

void findLv(nodep n) {

	if(n->child != NULL) { //se nao e node terminal
		nodep c = n->child;
		while(c != NULL) {
			if(c->child == NULL && (c->sdep - n->sdep == 1)) { //testar se edge terminal => c sem filhos e com sdep +1 == sdep do node
				Lv[n->id][c->Ti] = true;	
			}
			c = c->brother;
		}
	}

	if(n->child != NULL) {
		findLv(n->child);
	}

	if(n->brother != NULL) {
		findLv(n->brother);
	}
}

void buildLv(nodep rootp){

	Lv = (bool**)malloc(nodeCounter * sizeof(bool*));

	for (int i = 0; i < nodeCounter; i++) {
		Lv[i] = (bool*)malloc(nPatterns * sizeof(bool));
		for(int j = 0; j < nPatterns; j++) {
			Lv[i][j] = false;
		}
	}

	findLv(rootp);
}

void stackSearch(nodep n, stack<nodep>* stacks) {

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].push(n);
		}
	}

	if((n->child == NULL) && (n->head == 0)) { //node terminal com padrao inteiro
		for(int i = 0; i < nPatterns; i++) {
			nodep top = stacks[i].top();
			results[i][n->Ti] = max(top->sdep, results[i][n->Ti]);
		}
	}
//	else {
	if(n->child != NULL) {
		stackSearch(n->child, stacks);
	}
//	}

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].pop();
		}
	}

	if(n->brother != NULL) {
		stackSearch(n->brother, stacks);
	}
}

void stackDFS(nodep rootp) {

	stack<nodep> stacks [nPatterns];

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[rootp->id][i]) {
			stacks[i].push(rootp);
		}
	}

	stackSearch(rootp->child, stacks);

	for(int i = 0; i < nPatterns; i++) {
		results[i][i] = sizes[i]-1;
	}
}

void printResults() {
	for(int i = 0; i < nPatterns; i++) {
		for(int j = 0; j < nPatterns; j++) {
			cout <<  results[i][j] << " ";
		}
		cout << endl;
	}
}

void cleanDFS(nodep n) {

	if(n->child != NULL) {
		cleanDFS(n->child);
	}

	if(n->brother != NULL) {
		cleanDFS(n->brother);
	}

	free(n);

}

void cleanMem() {
	delete[](patterns);
	delete[](sizes);


	for (int i = 0; i < nPatterns; i++) {
		free(results[i]);
	}
	free(results);

	for (int i = 0; i < nodeCounter; i++) {
		free(Lv[i]);
	}
	free(Lv);

	cleanDFS(rootp);
}
/********** fim funcoes auxiliares **********/

/********** inicio do algortimo **********/

/*
   a medida que constroi ST_gen constroi lista L(v) para cada no.
   L contem o index i se path label para v é complete suffix de Si (tem terminador nas folhas do nivel imediatamente abaixo?)
   pode se construir L(v) depois da arvore

   percorrer ST_gen em DFS mantendo k(#S) pilhas 
   quando encontra no V poe em todas as stacks dos indices de L(v) V
   quando atingir no folha para Sj que represente toda a Sj, percorrer stacks e guardar para cada i o topo da pilha i: string-depth do topo é o comprimento da maior s-p de Si,Sj
   quando sobe fazer pop das stacks i em L(v)
 */

/*
   createST(patterns)
   constructL(st)
   DFS(st, L)
 */

/********** fim do algortimo **********/

/********** main **********/
   int main(){

   	readInput();

   	createRoot();

	for(int k = 0; k < nPatterns; k++) { //iterar todos os padroes
		patterns[k][sizes[k]-1] = '#'; //reset terminator

		string s = patterns[k]; 

		rootp->slink = slRootp;

		struct point point = createPoint(rootp);
		pointp p = &point;

		// cerr << "pattern " << k  << ": " << s << endl; 

		size_t j = 0;
		int head = 0;

		while(j < sizes[k]) { 
			// cerr << "trying to descend w/ " << s[j] << " from " << p->a->id << " + sdep: " << (p->s - p->a->sdep) << endl;

			while(!descendQ(p,s[j])) {
				// cerr << "nao deu pra descer de node: " << p->a->id << " +sdep=" << (p->s - p->a->sdep) << " w/ char " << s[j] << endl;
				nodep node = createNode(p,k,head);
				// cerr << endl;
				// cerr << "p actual: " << p->a->id << " +sdep=" << (p->s - p->a->sdep) << endl;
				suffixJump(p, node, k, j);
				// cerr << "p after jump: " << p->a->id << " +sdep=" << (p->s - p->a->sdep) << endl;
				head++;
				// cerr << endl;
				// cerr << "trying to descend w/ " << s[j] << " from " << p->a->id << " + sdep: " << (p->s - p->a->sdep) << endl;
			}

			descend(p,s[j]);
			// cerr << "just descended with index " << j << " s[j]: " << s[j] << ". j++" << endl;
			// cerr << "p: p->a=" << p->a->id << " and p->b=" << (p->b != NULL ? p->b->id : -2) << " and p->s=" << p->s << endl << endl;
			j++;
		}

		patterns[k][sizes[k]-1] = '$';
		// cerr << "PRINTING TREE FOR PATTERN=" << k << endl; printTree(rootp);
	}

	rootp->slink = slRootp;

	// cerr << "final tree" << endl; printTree(rootp);

	buildLv(rootp);
	// printLv();

	stackDFS(rootp);
	printResults();

	cleanMem();

	return 0;
}
