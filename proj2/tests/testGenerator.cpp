#include<iostream>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <assert.h>  
#include <stack>
#include <stdio.h>

using namespace std;

long nPatterns = 0;
long patternSize = 0;
char alfabeto[4] = {'A', 'C', 'T', 'G'};

void randomGen(char* pattern, int j) {
	int random = rand() % 4;
	pattern[j] = alfabeto[random];
}

void repeatGen(char* pattern, int j) {
	pattern[j] = 'A'; 
}


int main() {

	cerr << "número padroes?" << endl;
	cin >> nPatterns;
	cerr << "tamanho dos padroes ?" << endl;
	cin >> patternSize;

	cout << nPatterns << endl;

	for(int i = 0; i < nPatterns; i++) {
		char *pattern;
		pattern	= (char*) malloc ((patternSize+1) * sizeof(char));
		for(int j = 0; j < patternSize; j++) {
			//randomGen(pattern, j);
			repeatGen(pattern, j);
		}
		pattern[patternSize] = '\0';

		cout << patternSize << " " << pattern << endl;
	}

	cout << "X" << endl << nPatterns * patternSize << endl;

	return 0;
}
