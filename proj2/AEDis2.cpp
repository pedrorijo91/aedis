#include<iostream>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <assert.h>  
#include <stack>
#include <stdio.h>
#include <cstdlib>

using namespace std;

/********** declaracao de estruturas **********/
typedef struct node* nodep;
struct node {
	int id; //DEBUG
	int Ti; //id da string 
	int head; //primeiro caracter de patterns[Ti] a sair da root
	int sdep;
	nodep child;
	nodep brother; 
	nodep slink;
	nodep* hook;
};

struct point {
	nodep a;
	nodep b;
	int s; 
};
typedef struct point* pointp; 
/********** fim de estruturas **********/

/********** declaracao de funcoes **********/
// void createRoot();
/********** fim de declaracao de funcoes **********/

/********** declaracao de variaveis globais **********/
string* patterns;
size_t* sizes;
int nPatterns = 0;
int nodeCounter = 1;

nodep rootp;

bool** Lv;

int** results;
/********** fim de variaveis globais **********/

/********** funcoes debug **********/

int getNodeId() {
	int id = nodeCounter;
	nodeCounter++;
	return id;
}

void printNodep(nodep n) {
	cerr << "===================" << endl;
	cerr << "printing node" << endl;
	if(n != NULL) {
		cerr << "node ID: " << n->id << endl;
		cerr << "node Ti: " << n->Ti << endl;
		cerr << "node head: " << n->head << endl;
		cerr << "node sdep: " << n->sdep << endl;

		cerr << "node child: ";
		if(n->child != NULL) {
			cerr << n->child->id;
			cerr << " @ mem pos: " << n->child << endl;
		}
		else{
			cerr << "NULL" << endl;
		}

		cerr << "node brother: ";
		if(n->brother != NULL) {
			cerr << n->brother->id;
			cerr << " @ mem pos: " << n->brother << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node slink: ";
		if(n->slink != NULL) {
			cerr << n->slink->id << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node hook: ";
		assert(n->hook != NULL);
		// assert(*(n->hook) != NULL);
		if(n->hook != NULL && *(n->hook) != NULL) {
			cerr << (*(n->hook))->id;
			cerr << " @ mem pos: " << *(n->hook) << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
	}
	else {
		cerr << "node is NULL" << endl;
	}
	cerr << "finished printing node" << endl;
	cerr << "===================" << endl;
}

void printPointp(pointp p) {
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
	cerr << "printing point" << endl;

	cerr << "printing node above:" << endl;
	printNodep(p->a);
	cerr << "finished printing node above" << endl;

	cerr << "printing node bellow:" << endl;
	printNodep(p->b);
	cerr << "finished printing node bellow:" << endl;

	cerr << "point sdep: " << p->s << endl;

	cerr << "finished printing point" << endl;
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
}

void dfs(nodep p) {

	printNodep(p);
	if(p->child != NULL) {
		dfs(p->child);
	}
	else {
		cerr << "node without child" << endl;
	}
	if(p->brother != NULL) {
		dfs(p->brother);
	}
	else {
		cerr << "node without brother" << endl;
	}
}

void printTree(nodep root) {
	cerr << endl << endl << "PRINTING TREE" << endl;
	//realizar DFS
	printNodep(root);
	if(root->child != NULL) {
		dfs(root->child);
	}
	cerr <<"FINISHED TREE" << endl << endl; 
}

void printLv() {

	for(int i = 0; i < nodeCounter; i++) {
		for(int j = 0; j < nPatterns; j++) {
			cout << "Lv [" << i << "][" << j << "] : " << Lv[i][j] << endl;
		}
	}

}
/********** fim de funcoes debug **********/

/********** funcoes auxiliares **********/

void readInput() {

	cin >> nPatterns;

	patterns = new string[nPatterns]; 
	sizes = new size_t[nPatterns]; 

	string m;
	string pattern;
	for(int i = 0; i < nPatterns; i++) {
		patterns[i] = ""; 

		cin >> m;
		cin >> pattern; 

		patterns[i] = pattern + "$";
		sizes[i] = atoi(m.c_str()) + 1;
	}

	results = (int**)malloc(nPatterns * sizeof(int*));

	for (int i = 0; i < nPatterns; i++) {
		results[i] = (int*)malloc(nPatterns * sizeof(int));
		for(int j = 0; j < nPatterns; j++) {
			results[i][j] = 0;
		}
	}
}

void createRoot() {
	rootp=(struct node*) malloc(sizeof(struct node));

	rootp->id = 0;
	rootp->Ti = 0;
	rootp->head = 0;
	rootp->sdep = 0;
	rootp->child = NULL;
	rootp->brother = NULL;
	rootp->slink = rootp;
	rootp->hook = &(rootp);
}

struct point createPoint(nodep nodeA) {
	struct point p;
	p.s = nodeA->sdep;
	p.a = nodeA;
	p.b = nodeA->child;

	return p;
}

bool descendQ(pointp p, char c) {

	if(p->b == NULL) { //ńode terminal
		return false;
	}

	nodep n = p->a;

	if(n->sdep == p->s) { //se esta num node tem de testar os varios filhos
		nodep brother = n->child;
		while(brother != NULL) {
			if(patterns[brother->Ti][brother->head + p->s] == c) {
				return true;
			}
			brother = brother->brother;
		}
		return false;
	}
	else {
		return (patterns[p->b->Ti][p->b->head + p->s] == c);
	}
}

pointp descend(pointp p, char c) {

	if(p->s == p->a->sdep) { //se esta em node verificar qual filho desce
		nodep n = p->b;
		while(n != NULL) {
			if(patterns[n->Ti][n->head + p->s] == c) {
				p->b = n;
			}
			n = n->brother;
		}
	}

	p->s++;
	if(p->s == p->b->sdep) { //se quando descer chega ao node bellow
		p->a = p->b;
		p->b = p->b->child;
	}

	return p;
}

void descendSkip(pointp p, char c) {

	assert(p->a->id != p->b->id);

	nodep n = p->b;
	while(n != NULL) { //a partir de node verifica se pode chegar a qualquer um dos irmaos
		if(patterns[n->Ti][n->head + p->s] == c) {
			p->a = n;
			p->b = n->child;
			p->s = n->sdep;
			break;
		}
		n = n->brother;
	}
}

bool existsSL(nodep n, pointp p) {
	nodep sl = p->a->slink;

	int pattern = n->Ti;

	struct point point = createPoint(sl);
	pointp slp = &point;

	int slHead = n->head + 1;

	while(slp->s < (n->sdep - 1) && descendQ(slp, patterns[pattern][slHead + slp->s])) { //enquanto nao passar profundidade do node procura suffix link
		descendSkip(slp,patterns[pattern][slHead + slp->s]);
	}

	if(slp->s == (n->sdep - 1)) {
		n->slink = slp->a;
		return true;
	}

	return false;
}

void createNode(pointp p, int si, int head) {
	nodep leafParent = p->a;

	if(p->s > p->a->sdep) { //se estou no meio de nodes tenho de criar no em p
		struct node* n = (struct node*) malloc (sizeof *n);
		n->id = getNodeId();
		n->Ti = si;
		n->head = head;
		n->sdep = p->s; 
		n->child = p->b;
		n->hook = &(p->a->child);
		n->slink = NULL;


		{ //SLink block: assigns SL for current node and some waiting node
			if(rootp->slink != rootp) { //rootp contem node em espera que o seu SL seja criado
				rootp->slink->slink = n;
				rootp->slink = rootp;
			}
			if(!existsSL(n,p)) { //se SL nao existe guarda em espera na rootp
				rootp->slink = n;
			}
		}

		if(p->b != p->a->child) { // p nao e um ponto entre p->a e p->a->child (p->b nao e filho directo de p->a)
			n->brother = p->a->child;

			if(n->child != NULL) {
				*(n->child->hook) = n->child->brother;

				if(n->child->brother != NULL) {
					(n->child->brother->hook) = (n->child->hook);
				}

				n->child->brother = NULL; 

			}


			if(n->brother != NULL) {
				n->brother->hook = &n->brother;
			}

		}
		else {
			n->brother = n->child->brother;

			if(n->brother != NULL) {
				n->brother->hook = &(n->brother);
			}

			n->child->brother = NULL;

			*(n->child->hook) = n->child;

		}
		p->a->child = n;

		leafParent = n;
	}


	//criar folha
	struct node* leaf = (struct node*) malloc (sizeof *leaf);	
	leaf->id = getNodeId();
	leaf->Ti = si;
	leaf->head = head;
	leaf->sdep = patterns[si].length() - head;
	leaf->child = NULL;
	leaf->slink = NULL;

	leaf->hook = &(leafParent->child);
	leaf->brother = *(leaf->hook);

	*(leaf->hook)=leaf;

	if(p->b != NULL) {
		p->b->hook = &(leaf->brother);
	}

	leafParent->child = leaf;

	assert(*(leaf->hook)==leaf);
}

void findLv(nodep n) {

	if(n->child != NULL) { //se nao e node terminal
		nodep c = n->child;
		while(c != NULL) {
			if(c->child == NULL && (c->sdep - n->sdep == 1)) { //testar se edge terminal => c sem filhos e com sdep +1 == sdep do node
				Lv[n->id][c->Ti] = true;	
			}
			c = c->brother;
		}
	}

	if(n->child != NULL) {
		findLv(n->child);
	}

	if(n->brother != NULL) {
		findLv(n->brother);
	}
}

void buildLv(nodep rootp){

	Lv = (bool**)malloc(nodeCounter * sizeof(bool*));

	for (int i = 0; i < nodeCounter; i++) {
		Lv[i] = (bool*)malloc(nPatterns * sizeof(bool));
		for(int j = 0; j < nPatterns; j++) {
			Lv[i][j] = false;
		}
	}

	findLv(rootp);
}

void stackSearch(nodep n, stack<nodep>* stacks) {

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].push(n);
		}
	}

	if((n->child == NULL) && (n->head == 0)) { //node terminal com padrao inteiro
		for(int i = 0; i < nPatterns; i++) {
			if(!stacks[i].empty()) {
				nodep top = stacks[i].top();
				results[i][n->Ti] = max(top->sdep, results[i][n->Ti]);
			}
		}
	}
//	else {
	if(n->child != NULL) {
		stackSearch(n->child, stacks);
	}
//	}

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].pop();
		}
	}

	if(n->brother != NULL) {
		stackSearch(n->brother, stacks);
	}
}

void stackDFS(nodep rootp) {

	stack<nodep> stacks [nPatterns];

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[rootp->id][i]) {
			stacks[i].push(rootp);
		}
	}

	stackSearch(rootp->child, stacks);

	for(int i = 0; i < nPatterns; i++) {
		results[i][i] = sizes[i]-1;
	}
}

void printResults() {
	for(int i = 0; i < nPatterns; i++) {
		for(int j = 0; j < nPatterns; j++) {
			cout <<  results[i][j] << " ";
		}
		cout << endl;
	}
}

void cleanDFS(nodep n) {

	if(n->child != NULL) {
		cleanDFS(n->child);
	}

	if(n->brother != NULL) {
		cleanDFS(n->brother);
	}

	free(n);

}

void cleanMem() {
	delete[](patterns);
	delete[](sizes);


	for (int i = 0; i < nPatterns; i++) {
		free(results[i]);
	}
	free(results);

	for (int i = 0; i < nodeCounter; i++) {
		free(Lv[i]);
	}
	free(Lv);

	cleanDFS(rootp);
}
/********** fim funcoes auxiliares **********/

/********** inicio do algortimo **********/
/*
   a medida que constroi ST_gen constroi lista L(v) para cada no.
   L contem o index i se path label para v é complete suffix de Si (tem terminador nas folhas do nivel imediatamente abaixo?)
   pode se construir L(v) depois da arvore

   percorrer ST_gen em DFS mantendo k(#S) pilhas 
   quando encontra no V poe em todas as stacks dos indices de L(v) V
   quando atingir no folha para Sj que represente toda a Sj, percorrer stacks e guardar para cada i o topo da pilha i: string-depth do topo é o comprimento da maior s-p de Si,Sj
   quando sobe fazer pop das stacks i em L(v)
 */

/*
   createST(patterns)
   constructL(st)
   DFS(st, L)
 */
/********** fim do algortimo **********/

/********** main **********/
   int main(){
	// cout.setf(std::ios::boolalpha); //flag para imprimir booleanos em vez de int

   	readInput();

   	createRoot();

	for(int k = 0; k < nPatterns; k++) { //iterar todos os padroes
		patterns[k][sizes[k]-1] = '#'; //reset terminator

		string s = patterns[k]; 
		// cerr << "string " << k << ": " << s << endl;

		rootp->slink = rootp;

		for(size_t i = 0; i < sizes[k]; i++) { //iterar sufixos do padrao
			struct point point = createPoint(rootp);
			pointp p = &point;

			// cerr << "pattern " << k << ",substring " << i << ": " << s.substr(i) << endl;

			size_t j = 0;

			size_t suffixLength = s.length() - i;
			while(j < suffixLength && descendQ(p,s[i + j])) {
				// cerr << "possible to descend with " << s[i + j] << endl;
				p = descend(p,s[i + j]); 
				j++;
			}

			// cerr << "NOT possible to descend with index" << j << endl;

			createNode(p,k,i); 

			// cerr << "PRINTING TREE FOR i=" << i << endl;
			// printTree(rootp);
		}
		patterns[k][sizes[k]-1] = '$';
	}

//	cerr << "final tree" << endl; printTree(rootp);

	buildLv(rootp);
	// printLv();

	stackDFS(rootp);
	printResults();

	cleanMem();
	return 0;
}
