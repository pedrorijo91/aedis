#include<iostream>
#include <stdlib.h>
#include <cstring>
#include <vector>
#include <assert.h>  
#include <stack>
#include <stdio.h>

using namespace std;

/********** declaracao de estruturas **********/
typedef struct node* nodep;
struct node {
	int id; //DEBUG
	int Ti; //id da string 
	int head; //primeiro caracter de patterns[Ti] a sair da root
	int sdep;
	nodep child;
	nodep brother; 
	nodep slink;
	nodep* hook;
};

struct point {
	nodep a;
	nodep b;
	int s; 
};
typedef struct point* pointp; 
/********** fim de estruturas **********/

/********** declaracao de funcoes **********/
void createRoot();
/********** fim de declaracao de funcoes **********/

/********** declaracao de variaveis globais **********/
string* patterns;
size_t* sizes;
int nPatterns = 0;
int nodeCounter = 1;

nodep rootp;
nodep slRootp;

bool** Lv;

int** results;
/********** fim de variaveis globais **********/

/********** funcoes debug **********/

int getNodeId() {
	int id = nodeCounter;
	nodeCounter++;
	return id;
}

void printNodep(nodep n) {
	cerr << "===================" << endl;
	cerr << "printing node" << endl;
	if(n != NULL) {
		cerr << "node ID: " << n->id << endl;
		cerr << "node Ti: " << n->Ti << endl;
		cerr << "node head: " << n->head << endl;
		cerr << "node sdep: " << n->sdep << endl;

		cerr << "node child: ";
		if(n->child != NULL) {
			cerr << n->child->id;
			cerr << " @ mem pos: " << n->child << endl;
		}
		else{
			cerr << "NULL" << endl;
		}

		cerr << "node brother: ";
		if(n->brother != NULL) {
			cerr << n->brother->id;
			cerr << " @ mem pos: " << n->brother << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node slink: ";
		if(n->slink != NULL) {
			cerr << n->slink->id << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
		cerr << "node hook: ";
		assert(n->hook != NULL);
		// assert(*(n->hook) != NULL);
		if(n->hook != NULL && *(n->hook) != NULL) {
			cerr << (*(n->hook))->id;
			cerr << " @ mem pos: " << *(n->hook) << endl;
		}
		else{
			cerr << "NULL" << endl;
		}
	}
	else {
		cerr << "node is NULL" << endl;
	}
	cerr << "finished printing node" << endl;
	cerr << "===================" << endl;
}

void printPointp(pointp p) {
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
	cerr << "printing point" << endl;

	cerr << "printing node above:" << endl;
	printNodep(p->a);
	cerr << "finished printing node above" << endl;

	cerr << "printing node bellow:" << endl;
	printNodep(p->b);
	cerr << "finished printing node bellow:" << endl;

	cerr << "point sdep: " << p->s << endl;

	cerr << "finished printing point" << endl;
	cerr << "@@@@@@@@@@@@@@@@@@@" << endl;
}

void dfs(nodep p) {

	printNodep(p);
	if(p->child != NULL) {
		dfs(p->child);
	}
	else {
		cerr << "node without child" << endl;
	}
	if(p->brother != NULL) {
		dfs(p->brother);
	}
	else {
		cerr << "node without brother" << endl;
	}
}

void printTree(nodep root) {
	cerr << endl << endl << "PRINTING TREE" << endl;
	//realizar DFS
	printNodep(root);
	if(root->child != NULL) {
		dfs(root->child);
	}
	cerr <<"FINISHED TREE" << endl << endl; 
}

void printLv() {

	for(int i = 0; i < nodeCounter; i++) {
		for(int j = 0; j < nPatterns; j++) {
			cout << "Lv [" << i << "][" << j << "] : " << Lv[i][j] << endl;
		}
	}

}

/********** fim de funcoes debug **********/

/********** funcoes auxiliares **********/

void readInput() {

	cin >> nPatterns;

	patterns = new string[nPatterns]; 
	sizes = new size_t[nPatterns]; 

	string m;
	string pattern;
	for(int i = 0; i < nPatterns; i++) {
		patterns[i] = ""; 

		cin >> m;
		cin >> pattern; 

		patterns[i] = pattern + "$";
		sizes[i] = atoi(m.c_str()) + 1;
	}

	results = (int**)malloc(nPatterns * sizeof(int*));

	for (int i = 0; i < nPatterns; i++) {
		results[i] = (int*)malloc(nPatterns * sizeof(int));
		for(int j = 0; j < nPatterns; j++) {
			results[i][j] = 0;
		}
	}


	//DEBUG
	// for(int i = 0; i < nPatterns; i++) {
	// 	cerr << "READ INPUT - string " << i << ": " << patterns[i] << endl;
	// }
}

void aNode(nodep node)
{
	assert(*(node->hook) == node);

}


void createRoot() {
	slRootp = (struct node*) malloc(sizeof(struct node));

	slRootp->id = -1;
	slRootp->Ti = 0;
	slRootp->head = 0;
	slRootp->sdep = -1;
	slRootp->child = rootp;
	slRootp->brother = NULL;
	slRootp->slink = slRootp;
	slRootp->hook = &(slRootp->slink);	

	rootp = (struct node*) malloc(sizeof(struct node));

	rootp->id = 0;
	rootp->Ti = 0;
	rootp->head = 0;
	rootp->sdep = 0;
	rootp->child = NULL;
	rootp->brother = NULL;
	rootp->slink = slRootp;
	rootp->hook = &(slRootp->child);
}

struct point createPoint(nodep nodeA) {
	struct point p;
	p.s = nodeA->sdep;
	p.a = nodeA;
	p.b = nodeA->child;

	return p;
}

bool descendQ(pointp p, char c) {

	if(p->a == slRootp) {
		cerr << "descendQ from slRootp => true" << endl;
		return true;
	}

	if(p->b == NULL) {
		cerr << "descendQ: p->b is NULL. p->a=" << p->a->id << endl;
		return false;
	}

	nodep n = p->a;

	if(n->sdep == p->s) { //se esta num node tem de testar os varios filhos
		cerr << "descendQ: estou num node: " << n->id << endl;
		nodep brother = n->child;
		while(brother != NULL) {
			cerr << "descendQ: testing in descent to node: " << brother->id << " - chars: " << c << " and " << patterns[brother->Ti][brother->head + p->s] << endl;
			if(patterns[brother->Ti][brother->head + p->s] == c) {
				return true;
			}
			brother = brother->brother;
		}
		return false;
	}
	else {
		cerr << "descendQ: nao estou num node: " << n->id << " and " << p->b->id << " p->s:" << p->s << ". comp: " << patterns[p->b->Ti][p->b->head + p->s] << " and " << c << endl;
		return (patterns[p->b->Ti][p->b->head + p->s] == c);
	}
}

pointp descend(pointp p, char c) {

	if(p->a == slRootp) {
		p->a = rootp;
		p->b = p->a->child;
		p->s = 0;
	}
	else {
		if(p->s == p->a->sdep) { //se esta em node verificar qual filho desce
			cerr << "descend: estou num node " << p->a->id << " looking for " << c << endl;
			nodep n = p->b;
			while(n != NULL) {
				cerr << " testar direccao " << n->id << endl;
				if(patterns[n->Ti][n->head + p->s] == c) {
					cerr << "encontrei caminho em direccao a " << n->id << endl;
					p->b = n;
					break;
				}
				n = n->brother;
			}
		}

		p->s++;

		if(p->s == p->b->sdep) { //se quando descer chega ao node above
			//cerr << "descend: vou chegar a node" << endl;
			p->a = p->b;
			p->b = p->b->child;
		}
	}
	return p;
}

void descendSkip(pointp p, char c) {

	assert(p->a->id != (p->b != NULL ? p->b->id : -2));

	if(p->a == slRootp) {
		cerr << "descendSkip: estou na slRootp" << endl;
		p->a = rootp;
		p->b = p->a->child;
		p->s = 0;
	}
	else {
		cerr << "descendSkip: NAOO estou na slRootp: " << p->a->id << endl;
		nodep n = p->b;
		while(n != NULL) {
			if(patterns[n->Ti][n->head + p->s] == c) {
				p->a = n;
				p->b = n->child;
				p->s = n->sdep;
				// cerr << "descendSkip: encontrei descida !!!!!" << endl;
				break;
			}
			// cerr << "muda de brother" << endl;
			n = n->brother;
		}
		assert(n != NULL);
	}

}

bool existsSL(nodep n, pointp p) {
	nodep sl = p->a->slink;
	//	if(sl==NULL) {sl = p->a;}
	//	assert(sl != NULL);

	int pattern = n->Ti;

	struct point point = createPoint(sl);
	pointp slp = &point;

	cerr << "SL: looking for the sl of node " << n->id  << " from node " << sl->id  << " w/ sdep=" << n->sdep << endl;

	int slHead = n->head + 1;
	// cerr << "slhead: " << slHead << endl;

	while(slp->s < (n->sdep - 1) && descendQ(slp, patterns[pattern][slHead + slp->s])) {
		// cerr << "SL: possible to descend with " << patterns[pattern][slHead + slp->s] << " , slp->s=" << slp->s << " and n->sdep=" << n->sdep << endl;
		// slp = descend(slp,patterns[pattern][letter]);
		// cerr << "old point: " << endl;
		// printPointp(slp);
		descendSkip(slp,patterns[pattern][slHead + slp->s]);
		// cerr << "new point: " << endl;
		// printPointp(slp);
	}

	if(slp->s == (n->sdep - 1)) {
		cerr << "encontrei no com skip count trick, node " << n->id << " to SL node " << slp->a->id << endl;
		n->slink = slp->a;
		return true;
	}

	cerr << "existsSL: could not find SL for: " << n->id << endl;
	return false;
}

void createNode(pointp p, int si, int head) {
	nodep leafParent = p->a;

	if(p->s > p->a->sdep) { //se estou no meio de nodes tenho de criar no em p
		// cerr << "createNode: estou no meio de nodes - p->a: " << p->a->id << " e p->b: " << p->b->id << endl;
		struct node* n = (struct node*) malloc (sizeof *n);
		n->id = getNodeId();
		n->Ti = si;
		n->head = head;
		n->sdep = p->s; 
		n->child = p->b;
		n->hook = &(p->a->child);
		n->slink = NULL;


		{ //SLink block
			if(slRootp->slink != slRootp) {
				// cerr << "createNode: rootp contains node waiting for slink: " << slRootp->slink->id << ". slink will be " << n->id << endl;
				slRootp->slink->slink = n;
				slRootp->slink = slRootp;
			}
			//se no ja existir (p->a tem child com substring)
			//esse e SL
			//senao isto
			if(!existsSL(n,p)) {
				slRootp->slink = n;
				// cerr << "SL: sl node does not exits yet. node: " << n->id << endl;
			}
		}



		if(p->b != p->a->child) {
			// cerr << "createNode: p->b NOT child directo de p->a. p->a->child=" << p->a->child->id << endl;
			n->brother = p->a->child;

			if(n->child != NULL) {
				*(n->child->hook) = n->child->brother;

				if(n->child->brother != NULL) {
					// cerr << "createNode: n->child->brother != NULL " << n->child->brother->id << endl;
					// cerr << (*(n->child->hook))->id << endl;
					(n->child->brother->hook) = (n->child->hook);
				}

				n->child->brother = NULL; 

			}


			if(n->brother != NULL) {
				// cerr << "n->brother NOT NULL: " << n->brother->id << endl;
				n->brother->hook = &n->brother;
			}

		}
		else {
			// cerr << "createNode: p->b e child directo de p->a" << endl;
			// printNodep(n);
			n->brother = n->child->brother;

			if(n->brother != NULL) {
				// cerr << "n brother not null: " << n->brother->id << endl;
				n->brother->hook = &(n->brother);
				// printNodep(n->brother);
			}

			n->child->brother = NULL;

			*(n->child->hook) = n->child;

		}
		p->a->child = n;

		leafParent = n;

		cerr << "createNode: print do novo node: " << endl;
		cerr << "createNode: n->id " << n->id << " n-child: " << n->child->id << " n->brother: " << (n->brother != NULL ?  n->brother->id : -100) << " n->brother->brother: " << (n->brother != NULL && n->brother->brother != NULL ?  n->brother->brother->id : -100) << " n->hook: " << (n->hook != NULL  ?  (*(n->hook))->id : -100) << endl;

	// cerr << "createNode: print do no intermedio criado!!!" << endl;
	// printNodep(n);
	}
	else {
		// cerr << "createNode: nao estou no meio de nodes" << endl; //basta criar a folha
	}

	//	assert( (*(leafParent->hook)) == leafParent );

	//criar folha
	struct node* leaf = (struct node*) malloc (sizeof *leaf);	
	leaf->id = getNodeId();
	leaf->Ti = si;
	leaf->head = head;
	// leaf->sdep = p->s + s.length();
	leaf->sdep = patterns[si].length() - head;
	leaf->child = NULL;
	leaf->slink = NULL;

	leaf->hook = &(leafParent->child);
	leaf->brother = *(leaf->hook);

	*(leaf->hook)=leaf;

	if(p->b != NULL) {
		p->b->hook = &(leaf->brother);
	}

	leafParent->child = leaf;

	assert(*(leaf->hook)==leaf);

	// cerr << "createNode: print da folha criada!!!" << endl;
	// printNodep(leaf);
	// cerr << "createNode: print de nova leaf: " << endl;
	// cerr << "leafParent: " << leafParent->id << endl;
	cerr << "createNode: leaf->id " << leaf->id << " n->brother: " << (leaf->brother != NULL ?  leaf->brother->id : -100) << endl;
}

void suffixJump(pointp p, int pattern, int j) {
	nodep slp = p->a->slink;
	cerr << "making a SJ from node " << p->a->id << " with sl " << slp->id << ". needs to descend " << (p->s - p->a->sdep) << " letters" << endl;
	cerr << "pattern= " << pattern << " = " << patterns[pattern] << endl;

	int psdep = p->s;
	cerr << "psdep = " << psdep << endl;

	p->a = slp;
	p->s = slp->sdep;
	p->b = slp->child;

	//head = total - faltaLer - psdep
	int head = sizes[pattern] - psdep - (sizes[pattern] - j) + 1;
	if(slp == slRootp) {
		head--;
	}
	cerr << "SJ: head = " << head << endl;
	cerr << "p->a->child->sdep:" << (p->a->child != NULL ? p->a->child->sdep : -99) << endl;;
	cerr << "p->b->sdep:" << (p->b != NULL ? p->b->sdep : -999) << endl;
	while(p->s < psdep-1) {
		cerr << "descend with " << patterns[pattern][head + (p->s - slp->sdep)] << endl;
		descend(p,patterns[pattern][head + (p->s - slp->sdep)]);
	}

/*
	int pdep = p->s - p->a->sdep;
	cerr << " needs to descend " << pdep << " letters after find p->a->slink: " << p->a->slink->id << endl;
	cerr << "pattern= " << pattern << " = " << patterns[pattern] << endl;

	// int ndep = p->a->sdep;
	int head = p->a->child->head + 1;

	nodep nsl = p->a->slink; //precisamos de descer pdep letters daqui
	p->a = nsl;
	p->b = nsl->child;
	p->s = nsl->sdep;

	int i = 0;
	while(i < pdep) {
		cerr << i << ": descend with " << patterns[pattern][head + i] << endl;
		descend(p,patterns[pattern][head + i]);
		i++;
	}
	*/

	cerr << "SJ to point " << p->a->id << " + sdep " << (p->s - p->a->sdep) << " direction " << (p->b != NULL ? p->b->id : -100) << endl;

}

void findLv(nodep n) {

	if(n->child != NULL) {
		// cerr << "node contains children " << n->id << endl;
		nodep c = n->child;
		while(c != NULL) {
			// cerr << "testar L(v) no filho " << c->id << " para node " << n->id << endl;
			if(c->child == NULL && (c->sdep - n->sdep == 1)) {
				// cerr << "Lv[" << n->id << "][" << c->Ti << "] is true" << endl;
				Lv[n->id][c->Ti] = true;	
			}
			c = c->brother;
		}
	}
	else {
		// cerr << "node sem filhos! " << n->id << endl;
	}

	if(n->child != NULL) {
		// cerr << "find Lv on children " << n->child->id << " from node " << n->id << endl;
		findLv(n->child);
	}
	else {
		//Lv[n->id][n->Ti] = true;
	}

	if(n->brother != NULL) {
		// cerr << "find Lv on brother " << n->brother->id << " from node " << n->id << endl;
		findLv(n->brother);
	}
}

void buildLv(nodep rootp){

	Lv = (bool**)malloc(nodeCounter * sizeof(bool*));

	for (int i = 0; i < nodeCounter; i++) {
		Lv[i] = (bool*)malloc(nPatterns * sizeof(bool));
		for(int j = 0; j < nPatterns; j++) {
			Lv[i][j] = false;
		}
	}

	findLv(rootp);

}

void stackSearch(nodep n, stack<nodep>* stacks) {

	// cerr << "begin stackSearch with node " << n->id << endl;

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].push(n);
			// cerr << "stackSearch: fiz push do no " << n->id << " para stack " << i << endl;
		}
		else {
			// cerr << "stackSearch: caguei no node " << n->id <<" para stack " << i << endl;
		}
	}

	if((n->child == NULL) && (n->head == 0)) {
		//verificar pilhas
		// cerr << "node terminal " << n->id << " com padrao=" << patterns[n->Ti] << ", procurar pilhas" << endl;
		for(int i = 0; i < nPatterns; i++) {
			nodep top = stacks[i].top();
			// cerr << "stack " << i << ",node on top: " << top->id << ", suffix: " << patterns[i] << " and prefix: " << patterns[n->Ti] << " value: " << top->sdep << endl;
			// cerr << "vou inserir result em linha(suffix)=" << i << " e coluna(prefix)=" << n->Ti << endl;
			results[i][n->Ti] = max(top->sdep, results[i][n->Ti]);
		}
	}
	else {
		if(n->child != NULL) {
			// cerr << "procurar no(s) filhos" << endl;
			stackSearch(n->child, stacks);
		}
	}

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[n->id][i]) {
			stacks[i].pop();
		}
	}

	if(n->brother != NULL) {
		stackSearch(n->brother, stacks);
	}

}

void stackDFS(nodep rootp) {

	stack<nodep> stacks [nPatterns];

	for(int i = 0; i < nPatterns; i++) {
		if(Lv[rootp->id][i]) {
			stacks[i].push(rootp);
		}
	}

	// cerr << endl << "BEGINNING STACK DFS @@@@@@@@@@@@@@@@@@@@@@@@@@@2" << endl << endl;

	stackSearch(rootp->child, stacks);

	for(int i = 0; i < nPatterns; i++) {
		results[i][i] = sizes[i]-1;
	}
}

void printResults() {
	// cerr << endl << "PRINTING RESULTS" << endl << endl;
	for(int i = 0; i < nPatterns; i++) {
		for(int j = 0; j < nPatterns; j++) {
			// cerr << "SuffixPrefix of " << patterns[i] << " and " << patterns[j] << " = " << results[i][j] << endl;
			cout <<  results[i][j] << " ";
		}
		cout << endl;
	}
}

void cleanDFS(nodep n) {

	if(n->child != NULL) {
		cleanDFS(n->child);
	}

	if(n->brother != NULL) {
		cleanDFS(n->brother);
	}

	free(n);

}

void cleanMem() {
	delete[](patterns);
	delete[](sizes);


	for (int i = 0; i < nPatterns; i++) {
		free(results[i]);
	}
	free(results);

	for (int i = 0; i < nodeCounter; i++) {
		free(Lv[i]);
	}
	free(Lv);

	cleanDFS(rootp);
}
/********** fim funcoes auxiliares **********/

/********** inicio do algortimo **********/

/*
   a medida que constroi ST_gen constroi lista L(v) para cada no.
   L contem o index i se path label para v é complete suffix de Si (tem terminador nas folhas do nivel imediatamente abaixo?)
   pode se construir L(v) depois da arvore

   percorrer ST_gen em DFS mantendo k(#S) pilhas 
   quando encontra no V poe em todas as stacks dos indices de L(v) V
   quando atingir no folha para Sj que represente toda a Sj, percorrer stacks e guardar para cada i o topo da pilha i: string-depth do topo é o comprimento da maior s-p de Si,Sj
   quando sobe fazer pop das stacks i em L(v)
 */

/*
   createST(patterns)
   constructL(st)
   DFS(st, L)
 */

/********** fim do algortimo **********/

/********** main **********/
   int main(){
	cout.setf(std::ios::boolalpha); //flag para imprimir booleanos em vez de int

	readInput();

	createRoot();

	for(int k = 0; k < nPatterns; k++) { //iterar todos os padroes
		patterns[k][sizes[k]-1] = '#'; //reset terminator

		string s = patterns[k]; 
		// cerr << "string " << k << ": " << s << endl;

		rootp->slink = slRootp;

		// for(size_t i = 0; i < sizes[k]; i++) { //iterar sufixos do padrao
		struct point point = createPoint(rootp);
		pointp p = &point;

		// cerr << "pattern " << k << ",substring " << j << ": " << s.substr(i) << endl;
		cerr << "pattern " << k  << ": " << s << endl; 

		size_t j = 0;
		int head = 0;

		while(j < sizes[k]) { 
			cerr << "trying to descend w/ " << s[j] << " from " << p->a->id << " + sdep: " << (p->s - p->a->sdep) << endl;
			// printPointp(p);

			while(!descendQ(p,s[j])) {
				cerr << "p: p->a=" << p->a->id << " and p->b=" << (p->b != NULL ? p->b->id : -100) << endl;
				cerr << " nao foi possivel descer em p com " << s[j] << endl << endl;
				createNode(p,k,head);
				// cerr << "p->a->child: " << p->a->child->id << " = new node?" << endl;
				cerr << endl;
				suffixJump(p,k,j);
				cerr << endl;
				head++;

				// printPointp(p);
				cerr << "trying to descend w/ " << s[j] << " from " << p->a->id << " + sdep: " << (p->s - p->a->sdep) << endl;
			}

			descend(p,s[j]);
			cerr << "just descended with index " << j << " s[j]: " << s[j] << ". j++" << endl;
			cerr << "p: p->a=" << p->a->id << " and p->b=" << (p->b != NULL ? p->b->id : -100) << " and p->s=" << p->s << endl << endl;
			j++;
		}

		patterns[k][sizes[k]-1] = '$';
		cerr << "PRINTING TREE FOR PATTERN=" << k << endl; printTree(rootp);
		
	}

	rootp->slink = slRootp;

	cerr << "final tree" << endl; printTree(rootp);


	buildLv(rootp);
	// printLv();

	stackDFS(rootp);
	printResults();

	return 0;
}
